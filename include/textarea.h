#pragma once
#ifndef _HAMO_INCLUDED_
#error "please #include <hamo.h>"
#endif

struct textarea;
struct textarea_callback{
	void (*print)(void *t, const char *str);
};
DLLFUNC void message_new(void *klass, const struct textarea_callback *callback, const char *logfilename);
DLLFUNC void message_delete(void);
