#pragma once
#define _HAMO_INCLUDED_
#ifdef _HAMO_BUILD_
#define DLLFUNC __declspec(dllexport) 
#endif 
#ifdef _HAMO_LOAD_
#define DLLFUNC __declspec(dllimport) 
#endif
#ifdef _HAMO_STATICLINK_
#define DLLFUNC 
#endif
#include <stdint.h>
DLLFUNC int32_t hamo_main(int32_t c, char **v);
#include <textarea.h>
#include <progressbar.h>
