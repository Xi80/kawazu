#pragma once
#ifndef _HAMO_INCLUDED_
#error "please #include <hamo.h>"
#endif

struct progressbar_callback{
	void (*range_set)(void *t, int32_t index, const char *name, int32_t length);
	void (*add)(void *t, int32_t index, int32_t length);
	void (*ready)(void *t);
	void (*done)(void *t);
};

//called by CUI or GUI manager
DLLFUNC void progressbar_init(void *klass, const struct progressbar_callback *callback);

