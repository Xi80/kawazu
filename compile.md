# 動作環境
Msys2 でのみ確認. 現状 Windows のみで OS 依存のソースコードが一部含まれる.

# ビルド
現状 CUI に配布用 (`bin/hamo_cui.mak`), デバッグ用 (`bin/hamo_debug.mak`) の2種類. 配布用の DLL (`bin/hamo_dll.mak`) の合計 3 つのビルドがある. 

`bin/hamo_cui.mak` では `bin/hamo.dll` があればリンクして実行できる. しかし残りは mruby とシミュレータのための MCU 側のソースコードが必要で、このレポジトリのみではビルドはできない. 

将来的にこの問題に対応するが、それまで `bin/hamo.dll` はコンパイル済みのバイナリファイルをレポジトリに置く.

## bin/Makefile
3つの .mak を続けてビルドするので現状原作者のみ利用可能.

## CUI 配布用
```
make -C kawazu/bin -f hamo_dll.mak
```

このビルドは3つのソースファイルだけでコールバック関数を実装している. GUI 版作成の参考にしてほしい.

## GUI
GUI 版では bin/hamo.dll をリンクして利用できる. source_gui にソースを配置すること. 

