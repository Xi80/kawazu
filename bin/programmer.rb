module Programmer
	FLASH_STATUS_FIFO_READY = (0 << 0)
	FLASH_STATUS_FIFO_BUSY = (1 << 0)
	FLASH_STATUS_PROGRAMMING_READY = (0 << 1)
	FLASH_STATUS_PROGRAMMING_BUSY = (1 << 1)
	FLASH_STATUS_ERASING = (1 << 2)
	FLASH_STATUS_FIFO_EMPTY_SUSPENDED = (1 << 3)
	FLASH_STATUS_ERASE_ERROR = (1 << 4)

	FLASH_STATUS_DONE_MASK = FLASH_STATUS_PROGRAMMING_BUSY | FLASH_STATUS_ERASING | FLASH_STATUS_FIFO_EMPTY_SUSPENDED
class Fifo
	def initialize(region, src)
		@region = region
		@src = src
	end
	def command_flash_fifo_set(action, src = @src)
		if src.size == 0
			case action
			when 1
				return
			when 2
				xxx
			end
		end
		h = Command.header_send Command::NUMBER_FLASH_FIFO_DATA_SEND, [@region | (action << 2), 0], [0xff00, src.size].min, 0, false
		if h == nil
			return
		end
		if h[:pc_to_mcu] == 0
			return
		end
		serialport_send(src.slice!(0, h[:pc_to_mcu]))
		printf("append:%d, size:%d ", h[:pc_to_mcu], src.size)
		j = Command.header_receive(false)
		if j[:increment] == h[:increment]
			puts("same header...")
			j = Command.header_receive(false)
			if j[:command_number] != Command::NUMBER_FLASH_FIFO_DATA_SEND
				puts("starnge command number $%02x", j)
				xxx
			end
		end
		j
	end
	def command_flash_fifo_clear
		command_flash_fifo_set(0, [])
	end

	def command_flash_fifo_new
		command_flash_fifo_set(1)
	end
	def command_flash_fifo_append(statuses)
		h = command_flash_fifo_set(2)
		statuses.concat(serialport_receive(h[:mcu_to_pc]))
	end
	def empty?
		@src.size == 0
	end
end

class ProgrammerBase
	def command_processor_address_set(t)
		r = t[:processor][0].begin 
		r |= t[:absolute] & (t[:processor][0].size - 1)
		r
	end
	def initialize(driver, param, rom)
		@driver = driver
		@c_5555 = command_processor_address_set(param[:c_5555])
		@c_2aaa = command_processor_address_set(param[:c_2aaa])
		@command_address_5555 = param[:c_5555]
		@command_address_2aaa = param[:c_2aaa]
		@dest = param[:c_dest]
		@dest_processor_address = @dest[:processor][0].begin
		@skip_data_0xff = param[:skip_data_0xff]
		@flashdevice_page_size = param[:program_page_size]
		@erase_request = param[:erase_request]
		@erase_sector = param[:erase_sector]
		@erase_supported = param[:erase_supported]
		@fifo = nil
		@programming_first_time = true
		@programming_done = false
		@rom = rom
	end
	def done?
		@programming_done
	end
	def erase_type_accepted?
		if @erase_request == :erase_type_none
			return true
		end
		r = @erase_supported.include? @erase_request
		if r == false
			puts debug_name + ": requested erase type is not supported."
			return false
		end
		r
	end
	def command_fifo_new
		if @rom.size == 0
			@programming_done = true
			return
		end
		@fifo = Fifo.new(@region_write == Command::REGION_CPU_FLASH ? 0 : 1, @rom.dup)
		@fifo.command_flash_fifo_new
	end
	def erase_branch
		case @erase_request
		when :erase_type_none
			#nothing todo
		when :erase_type_entire
			if @programming_first_time
				return :erase_type_entire
			end
		when :erase_type_sector 
			r = @erase_sector.reject!{|t|
				t.include? @dest[:absolute]
			}
			if r != nil
				return :erase_type_sector 
			end
		else
			xxx
		end
		return :erase_type_none
	end
	def content_flash_erase(c)
		chip_erase = erase_branch()
		if chip_erase == :erase_type_none
			return
		end
		chip_erase = chip_erase == :erase_type_entire
		address = {:type => Command::TYPE_RANDOM, :value => [
			@c_5555, @c_2aaa, @c_5555, @c_5555, @c_2aaa, 
			chip_erase ? @c_5555 : @dest_processor_address
		]}
		data = {:type => Command::TYPE_RANDOM, :value => [
			0xaa, 0x55, 0x80, 0xaa, 0x55, 
			chip_erase ? 0x10 : 0x30
		]}
		Command.content_access_set(c, address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_ERASE)
	end
	def command_rw(c, notify_use)
		flag = [0, 0]
		if @region_write == Command::REGION_CPU_FLASH
			flag[0] |= 1 << 6
		else
			flag[1] |= 1 << 6
		end
		if notify_use
			flag[1] |= 1 << 4
		end
		Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, flag)
	end
	def program_content_make_page(c)
		address = {}
		address[:type] = Command::TYPE_SEQUENTIAL
		address[:value] = [@dest_processor_address]
		data = {}
		data[:type] = Command::TYPE_FIFO_PAGE
		data[:value] = [0xde]
		Command.content_access_set(c, @flashdevice_page_size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_PROGRAMMING)
	end
	def program_content_make_write(c)
		address = {:type => Command::TYPE_RANDOM, :value => [@c_5555, @c_2aaa, @c_5555, @dest_processor_address]}
		data = {:type => Command::TYPE_RANDOM, :value => [0xaa, 0x55, 0xa0, 0xdd]}
		if @flashdevice_page_size != 1
			address[:value].pop
			data[:value].pop
		end
		Command.content_access_set(c, address[:value].size, @region_write, Command::RW_WRITE_WITH_DATA, address, data, Command::FLASH_COMMAND_PROGRAMMING)
		if @flashdevice_page_size == 1
			return
		end
		program_content_make_page(c)
	end
	def log2_fixnum(val)
		if val == 0
			xxx
		end
		pow = 0
		while val != 1
			val >>= 1
			pow += 1
		end
		pow
	end
	def program_content_make_read(c, flag)
		address = {:type => Command::TYPE_FIXED, :value => [@dest_processor_address]}
		Command.content_access_set(c, 2, @region_read, Command::RW_READ, address, nil, Command::FLASH_COMMAND_POLLING)
		f = (log2_fixnum(@dest[:combined_bank].size) - 8) << 4
		if @flashdevice_page_size != 1
			f |= (log2_fixnum(@flashdevice_page_size) - 4) << 1
		end
		if @skip_data_0xff
			f |= 1
		end
		flag[status_index] = f
	end

	def content_random_write_set(c, region, a, d, flash_command = Command::FLASH_COMMAND_NONE)
		address = {:type => Command::TYPE_RANDOM, :value => a}
		data = {:type => Command::TYPE_RANDOM, :value => d} 
		Command.content_access_set(c, address[:value].size, region, Command::RW_WRITE_WITH_DATA, address, data, flash_command)
	end
	def content_dest_bank_switch(c)
		i = 0
		@dest[:processor].each{|t|
			@driver.bank_switch(c, @region_write, t.begin, @dest[:absolute] + i)
			i += t.size
		}
	end
	def content_memorybank_update(c)
		if @programming_first_time
			if @command_address_2aaa[:switch]
				@driver.bank_switch(c, @region_write, 
					@command_address_2aaa[:combined_bank].begin,
					@command_address_2aaa[:absolute]
				)
			end
			if @command_address_5555[:switch]
				@driver.bank_switch(c, @region_write, 
					@command_address_5555[:combined_bank].begin,
					@command_address_5555[:absolute]
				)
			end
		end
		content_dest_bank_switch(c)
	end
	def command_memorybank_update_program_start(notify_use)
		c = Command.content_new
		content_memorybank_update(c)
		content_flash_erase(c)
		command_rw(c, notify_use)
	end
	def status_idle?(status)
		(status & FLASH_STATUS_DONE_MASK) == 0
	end
	def programming_loop(statuses, notify_use)
		#if @programming_done
		#	return
		#end
		status = statuses[self.status_index()]
		if @fifo.empty? && status_idle?(status)
			@programming_done = true
			return
		end
		if @fifo.empty?
			if (status & FLASH_STATUS_FIFO_EMPTY_SUSPENDED) != 0
				#この if は kludge なので根本的修正ができたら直す
				@programming_done = true
			end
			return
		end
		if status_idle?(status)
			if (status & FLASH_STATUS_FIFO_BUSY) == 0
				@fifo.command_flash_fifo_append(statuses)
				2.times{
					statuses.shift
				}
			end
			if @programming_first_time == false
				@dest[:absolute] += @dest[:combined_bank].size
			end
			str = self.debug_name + '.abs '
			str += "$%05X" % @dest[:absolute]
			puts str
			command_memorybank_update_program_start(notify_use)
			@programming_first_time = false
		elsif (
			(status & FLASH_STATUS_FIFO_BUSY) == 0 ||
			(status & FLASH_STATUS_FIFO_EMPTY_SUSPENDED) != 0
		)
			@fifo.command_flash_fifo_append(statuses)
			p statuses
		end
	end
	
	def compare
		if @rom.size == 0
			return
		end
		programmed_data = []
		@dest[:absolute] = 0
		while @dest[:absolute] < @rom.size
			c = Command.content_new
			content_dest_bank_switch(c)
			Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, [0, 0])
			programmed_data += Memory.read(@region_read, @dest[:combined_bank].begin, @dest[:combined_bank].size)
			@dest[:absolute] += @dest[:combined_bank].size
		end
		#f = File.open(debug_name + ".bin", 'wb')
		#f.write(programmed_data.pack('C*'))
		#f.close
		Programmer.memory_compare(debug_name, @rom, programmed_data)
	end
	
	def clear
		@fifo.command_flash_fifo_clear
	end
end

class ProgrammerCpu < ProgrammerBase
	def initialize(d, param, rom)
		@region_read = Command::REGION_CPU_6502
		@region_write = Command::REGION_CPU_FLASH
		super(d, param, rom)
	end
	def cartridge_init
		@driver.workram_control_set(false)
		@driver.irq_disable
	end
	def debug_name
		"CPU"
	end
	def status_index
		0
	end
end
class ProgrammerPpu < ProgrammerBase
	def initialize(d, param, rom)
		@region_read = Command::REGION_PPU
		@region_write = Command::REGION_PPU
		super(d, param, rom)
	end
	def debug_name
		"PPU"
	end
	def status_index
		1
	end
end

	def self.flash_programming_main(cpu, ppu, notify_use, yield_count = 0)
		cpu.cartridge_init
		programmer = [cpu, ppu]
		programmer.each{|t|
			t.command_fifo_new
		}
		Command.flash_program_program(programmer)
		
		#programming loop
		status = Command.flash_fifo_status_get
		programmer.each{|t|
			t.programming_loop(status, notify_use)
			status = Command.flash_fifo_status_get
			if (status[0] & FLASH_STATUS_ERASE_ERROR) != 0  || (status[1] & FLASH_STATUS_ERASE_ERROR) != 0
				puts 'erase error!!'
				xxx
			end
		}
		while !cpu.done? || !ppu.done?
			if status.size > 2
				status = status[-2, 2]
			elsif notify_use == false
				usleep(25 * 1000) #100 ms
				status = Command.flash_fifo_status_get
				p status
			else
				if notify_use && yield_count != 0
					yield_count.times{
						Fiber.yield
					}
				end
				status = Command.fifo_notify_get
			end
			if $dma_puts
				printf "status [$%02x,$%02x]\n", status[0], status[1]
			end
			if status == nil
				break
			end
			if (status[0] & FLASH_STATUS_ERASE_ERROR) != 0  || (status[1] & FLASH_STATUS_ERASE_ERROR) != 0
				puts 'erase error!!'
				xxx
			end
			programmer.each{|t|
				t.programming_loop(status, notify_use)
			}
		end
		programmer.each{|t|
			t.clear
		}
		programmer.each{|t|
			t.compare
		}
	end

	def self.memory_compare(name, src, dest)
		if src == dest
			puts name + " programming ok!"
			return
		end

		puts name + " programming failed..."
		offset = 0
		l = src.size
		while l >= 0x400
			offset = 0
			str = ''
			while offset < src.size
				diff = 0
				l.times{|i|
					if src[offset + i] != dest[offset + i]
						diff += 1
					end
				}
				if diff != 0
					str += sprintf("$%05x-$%05x:%d ", offset, offset+l-1, diff)
				end
				offset += l
			end
			if str != ''
				puts str.chomp
			end
			l /= 2
		end
		diff = 0
		str = ""
		src.size.times{|i|
			if src[i] != dest[i]
				diff += 1
				str += sprintf("$%05x:%02x_%02x ", i, src[i], dest[i])
				if str.size >= 60
					puts str
					str = ''
				end
			end
			if diff >= 0x20
				break
			end
		}
		if str != ''
			puts str
		end
		if diff >= 0x20
			puts "and more..."
		end
	end
end
