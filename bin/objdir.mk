$(OBJDIR):
	mkdir $@
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<
$(OBJDIR)/mruby_simulator.o: $(SRCDIR)/mruby_simulator.c
	$(CC) $(CFLAGS) -I$(SBP_DIR)/include -DSYSTEM_SIM -c -o $@ $<
-include $(OBJDIR)/*.d
