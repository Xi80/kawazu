module Message
extend self
	def puts(str)
		message_print str + "\n"
	end
	def print(str)
		message_print str
	end
	def printf(format, *arg)
		message_print(sprintf(format, *arg))
	end
end
