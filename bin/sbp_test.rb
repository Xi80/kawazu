COMPORT_DEFAULT = "COM3"
#require './debug.rb'
require './sbp_serialport.rb'
require './sbp_memoryaccess.rb'
require './sbp_binary_command.rb'
require './programmer.rb'
require './flashmemory_driver.rb'
require './nesfile.rb'
require './cartridge_driver.rb'
require './mmc1_sxrom.rb'
require './mmc3_txrom.rb'
require './gplogic_74161.rb'

def sbp_ppu_bus_test(arg)
	if arg.size == 0
		arg << COMPORT_DEFAULT
	end
	if sbp_open(arg.shift) == false
		serialport_close()
		return
	end
	puts "CHARA RAM detection: " + ppu_chararam_detection.to_s
	sbp_close()
end

def driver_test(arg)
	t = DriverTxROM.new
	t.rom_dump
end
def sbp_temp_test(arg)
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
=begin
	4.times{|i|
		Memory.write(Command::REGION_CPU_6502, 0x8000, [6, i])
		d = Memory.read(Command::REGION_CPU_6502, 0x8000, 0x2000)
		dump('>', d[0, 0x40])
	}
=end
	sbp_close()
end

def sbp_lz93d50_test(arg)
	destfilename = arg.shift
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end

	4.times{|i|
		c = Command.content_new
		address = {:type => Command::TYPE_RANDOM, :value => [0x8000]}
		data = {:type => Command::TYPE_RANDOM, :value => [i]}
		Command.content_access_set(c, data[:value].size, Command::REGION_CPU_6502, Command::RW_WRITE_WITH_DATA, address, data)
		
		address[:value] = [0x0000]
		address[:type] = Command::TYPE_RANDOM
		#Command.content_access_set(c, 1, Command::REGION_CPU_6502, Command::RW_READ, address)
		Command.content_access_set(c, 1, Command::REGION_PPU, Command::RW_READ, address, data)
		
		address = {:type => Command::TYPE_RANDOM, :value => [0x8000]}
		data = {:type => Command::TYPE_RANDOM, :value => [i ^ 1]}
		#Command.content_access_set(c, data[:value].size, Command::REGION_CPU_6502, Command::RW_WRITE_WITH_DATA, address, data)
		address[:value] = [0x0000]
		#Command.content_access_set(c, 1, Command::REGION_PPU, Command::RW_WRITE_WITH_DATA, address, data)
		
		d = Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, [0, 0])
		
		d = memory_read(Command::REGION_PPU, 0x0100, 8)
		#d = memory_read(Command::REGION_CPU_6502, 0x8000, 8)
		dump(i.to_s + '>', d)
	}
	sbp_close()
	return

	cpurom = {:rom => [], :length => 0x40000, :pagesize => 0x4000}
	0x10.times{|i|
		c = Command.content_new
		address = {:type => Command::TYPE_RANDOM, :value => [0x8008]}
		data = {:type => Command::TYPE_RANDOM, :value => [i]}
		Command.content_access_set(c, data[:value].size, Command::REGION_CPU_6502, Command::RW_WRITE_WITH_DATA, address, data)
		
		address[:value] = [0x8000]
		address[:type] = Command::TYPE_SEQUENTIAL
		Command.content_access_set(c, 1, Command::REGION_CPU_6502, Command::RW_READ, address)
		d = Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, [0, 0])
		
		cpurom[:rom] += memory_read(Command::REGION_CPU_6502, 0x8000, 0x4000)
	}

	ppurom = {:rom => [], :length => 0x40000, :pagesize => 0x400}
	0.step(0x100 - 1, 8){|i|
		8.times{|j|
			c = Command.content_new
			address = {:type => Command::TYPE_RANDOM, :value => [0x8000 + j]}
			data = {:type => Command::TYPE_RANDOM, :value => [i + j]}
			Command.content_access_set(c, data[:value].size, Command::REGION_CPU_6502, Command::RW_WRITE_WITH_DATA, address, data)
			
			address[:value] = [0x8000]
			address[:type] = Command::TYPE_SEQUENTIAL
			Command.content_access_set(c, 1, Command::REGION_CPU_6502, Command::RW_READ, address)
			d = Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, [0, 0])
		}
		
		ppurom[:rom] += memory_read(Command::REGION_PPU, 0x0000, 0x0400 * 8)
	}


	nesfile_save(cpurom, ppurom, 16, destfilename)
	sbp_close()
end

def unrom_noconflict_dump(arg)
	destfilename = arg.shift
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
	cpurom = {:rom => [], :length => 0x40000, :pagesize => 0x4000}
	0x0f.times{|i|
		memory_write(Command::REGION_CPU_6502, 0x8008, [i])
		cpurom[:rom] += memory_read(Command::REGION_CPU_6502, 0x8000, 0x4000)
	}
	cpurom[:rom] += memory_read(Command::REGION_CPU_6502, 0xc000, 0x4000)
	nesfile_save(cpurom, {:rom => nil, :pagesize=>0x4000}, 16, destfilename)
	sbp_close()
end

def sbp_mmc1_rom_dump(arg)
	destfilename = arg.shift
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
	mmc1_rom_dump(destfilename)
	sbp_close()
end

def sbp_mmc3_rom_dump(arg)
	destfilename = arg.shift
	mappernumber = arg.shift
	if mappernumber == nil
		mappernumber = 4
	end
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
	d = DriverTxROM.new
	d.rom_dump(destfilename)
	sbp_close()
end

def memory_detection(region_read, region_write, c_0000, c_2aaa, c_5555)
	name = "CPU"
	if region_read != Command::REGION_CPU_6502
		name = "PPU"
	end
	rom = memory_read(region_read, c_0000, 3) #本来は2だが、1と2は MCU が停まってしまうので要調査
	id = Command.flash_id_read(region_read, region_write, c_0000, c_2aaa, c_5555)
	if rom[0,2] == id
		puts name + " memory: ROM"
	else
		printf "%s memory: flash, id: %02X %02X\n", name, id[0], id[1]
	end
end

def sbp_mmc3_flash_id_get(arg)
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
	#memory_read(Command::REGION_CPU_6502, 0x8000, 0x4000)
	c_0000 = 0x8000
	c_2aaa = (0x2aaa & 0x1fff) | 0xa000
	c_5555 = (0x5555 & 0x3fff) | 0xc000
	memory_write(Command::REGION_CPU_6502, 0x8000, [6, 0 / 0x2000, 7, 0x2aaa / 0x2000], 0x8001)
	memory_detection(Command::REGION_CPU_6502, Command::REGION_CPU_FLASH, c_0000, c_2aaa, c_5555)
	if ppu_chararam_detection == true
		puts "PPU memory: RAM"
		sbp_close()
		return
	end
	
	c_0000 = 0x0000
	c_2aaa = (0x2aaa & 0x0fff) | 0x0800
	c_5555 = (0x5555 & 0x03ff) | 0x1000
	memory_write(Command::REGION_CPU_6502, 0x8000, [0, 0 / 0x0800, 1, 0x2aaa / 0x0800, 2, 0x5555 / 0x0400], 0x8001)
	memory_detection(Command::REGION_PPU, Command::REGION_PPU, c_0000, c_2aaa, c_5555)
	sbp_close()
end

def sbp_address_test(arg)
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end
	a = {:type => Command::TYPE_RANDOM, :value => []}
	0x10.times{|i|
		a[:value] << (0xf000 | ((i^0xf) << 8))
	}
	d = {:type => Command::TYPE_FIXED, :value => [0]}
	command_write(Command::REGION_CPU_6502, a[:value].size, a, d)
	sbp_close()
end
def sbp_flash_program(arg)
	srcfilename = arg.shift
	portname = arg.shift
	if portname == nil
		portname = COMPORT_DEFAULT
	end
	if sbp_open(portname) == false
		return
	end

	cpu = {}; ppu = {}
	cpu[:memory] = nil
	ppu[:memory] = nil
	default_device = "AM29F040B"

	r = Nesfile.Load(srcfilename)
	if r == nil
		puts 'nes file header error'
		return
	end
	#r[:cpu_program][:data] = [] #r[:cpu_program][:data][0, 0x4000]
	r[:ppu_chara][:data] = [] #r[:ppu_chara][:data][0, 0x4000]

	cpu[:parameter] = {
		:dest_abs_address => 0,
		:erase_request => :erase_type_sector
		#:erase_request => :erase_type_entire
	}
	ppu[:parameter] = {
		:dest_abs_address => 0,
		:erase_request => :erase_type_sector
	}
	[cpu, ppu].each{|t|
		if t[:memory] == nil
			t[:memory] = default_device
		end
		t[:memory] = sbp_flash_get(t[:memory])
		t[:memory].parameter_get(t[:parameter])
	}
	case r[:mappernum]
	when 1
		cpu[:programmer] = ProgrammerCpuSxROM.new(cpu[:parameter], r[:cpu_rom])
		ppu[:programmer] = ProgrammerPpuSxROM.new(ppu[:parameter], r[:ppu_rom])
		cpu[:programmer].ppu_programmer_set(ppu[:programmer])
		ppu[:programmer].cpu_programmer_set(cpu[:programmer])
	when 4
		d = DriverTxROM.new
		d.flash_program_init_cpu(cpu, r)
		d.flash_program_init_ppu(ppu, r)
	end
	[cpu, ppu].each{|t|
		if t[:programmer].erase_type_accepted? == false
			return
		end
	}

	Programmer.flash_programming_main(cpu[:programmer], ppu[:programmer], true)
	sbp_close()
end
