module Command
	NUMBER_EXIT = 2
	NUMBER_BUS_ACCESS_START = 0x11
	NUMBER_BUS_ACCESS_PROGRAM = 0x12
	NUMBER_FLASH_STATUS_GET = 0x13
	NUMBER_FLASH_FIFO_DATA_SEND = 0x14
	NUMBER_FLASH_STATUS_NOTIFY = 0x15
	
	REGION_CPU_6502 = 0; REGION_CPU_FLASH = 1
	REGION_PPU = 2; REGION_VRAM_CONNECTION = 3

	CONTENT_CONTINUE = 0b01; CONTENT_END = 0b11
	CONTENT_TYPE_BUSCONTROL = 0; CONTENT_TYPE_DATA_WRITE = 1;
	CONTENT_TYPE_ADDRESS = 2; CONTENT_TYPE_DATA_READ = 3

	RW_READ = 0; RW_WRITE_WITH_DATA = 1; RW_WRITE_WITHOUT_DATA = 2

	TYPE_RANDOM = 0; TYPE_SEQUENTIAL = 1; TYPE_FIXED = 2; TYPE_FIFO_PAGE = 3
	TYPE_READ_DUMMY = 0; TYPE_READ_VALID = 1

	STATUS_NEXT_COMMAND_READY = 1 << 6
	STATUS_DIR_PC_TO_MCU = 0 << 5; STATUS_DIR_MCU_TO_PC = 1 << 5
	STATUS_TYPE_COMMAND = 0 << 4; STATUS_TYPE_DATA = 1 << 4
	STATUS_ERROR = 1 << 3
	STATUS_READY = 0; STATUS_REQUEST = 1; STATUS_BUSY = 2
	STATUS_SEND = 3; STATUS_DONE = 4; STATUS_ABORT = 5

	FLASH_COMMAND_NONE = 0; FLASH_COMMAND_ERASE = 1
	FLASH_COMMAND_POLLING = 2; FLASH_COMMAND_PROGRAMMING = 3

extend self
	def le16_pack(val)
		ar = []
		ar << (val & 0xff)
		val >>= 8
		ar << (val & 0xff)
		ar
	end

	def le16_unpack(ar)
		ar.pack('C*').unpack('v1')[0]
	end

	def checksum_set(d)
		sum = 0xff
		d.each{|t|
			sum += t
		}
		sum &= 0xff
		sum ^= 0xff
		d << sum
	end
	def content_member_set(c, length, dest_member, content_type, spi_data, flash_command, data_pack_prefix = "C")
		c[dest_member][:content] << {
			:length => length, :content_type => content_type, 
			:spi_data => spi_data.dup, :flash_command =>flash_command, :data_pack_prefix => data_pack_prefix
		}
		if spi_data[:dummy] == false
			c[dest_member][:dummy] = false
		end
	end

	def content_access_set(c, length, region, rw, address, spi_data = nil, flash_command = FLASH_COMMAND_NONE)
		#bus control
		bt = {:type => TYPE_FIXED, :value => [(region << 6) | (rw << 4)], :dummy => false}
		content_member_set(c, length, :buscontrol, CONTENT_TYPE_BUSCONTROL, bt, flash_command)
		#data write
		if spi_data == nil
			spi_data = {:value => [0x3a], :type => TYPE_FIXED, :dummy => true}
		else
			spi_data[:dummy] = false
		end
		content_member_set(c, length, :data_write, CONTENT_TYPE_DATA_WRITE, spi_data, 0)
		#address
		address[:dummy] = false
		content_member_set(c, length, :address, CONTENT_TYPE_ADDRESS, address, 0, 'v')
		#data read
		data_read = {:value => [], :type => TYPE_READ_DUMMY, :dummy => true}
		if rw == RW_READ
			data_read[:type] = TYPE_READ_VALID
			data_read[:dummy] = false
		end
		content_member_set(c, length, :data_read, CONTENT_TYPE_DATA_READ, data_read, flash_command)
	end

	def content_new
		h = {}
		c = {:length_list=>h}
		[:buscontrol, :data_write, :address, :data_read].each{|k|
			c[k] = {:dummy => true, :content => []}
		}
		c
	end
	def content_organize(c, value_mask)
		dest = []
		c[:content].each{|t|
			length = t[:length]
			a = t[:spi_data][:value][0]
			randomized_value = nil
			case t[:spi_data][:type]
			when TYPE_SEQUENTIAL
				if length > 0x10
					dest << t
					next
				end
				randomized_value = []
				length.times{
					randomized_value << a
					a += 1
					a &= value_mask
				}
			when TYPE_FIXED
				if dest.size != 0 && dest[-1][:spi_data][:type] == TYPE_FIXED && dest[-1][:spi_data][:value][-1] == a
					dest[-1][:length] += length
					printf "v:$%02x l:$%02x ", a, dest[-1][:length]
					next
				end
				randomized_value = Array.new(length, a)
			when TYPE_RANDOM
				randomized_value = t[:spi_data][:value]
			when TYPE_FIFO_PAGE
				dest << t
				next
			end
			
			if dest.size == 0 || dest[-1][:spi_data][:type] != TYPE_RANDOM

=begin
		 || (
			dest[-1][:spi_data][:type] == TYPE_RANDOM &&
			!c[:length_list].key?(dest[-1][:length] + length)
		)
=end
				t[:spi_data][:type] = TYPE_RANDOM
				t[:spi_data][:value] = randomized_value
				dest << t
				next
			end
			dest[-1][:spi_data][:value] += randomized_value
			dest[-1][:length] += length
		}
		c[:content] = dest
	end

	def content_print(s)
		puts '---'
		s[:content].each{|t|
			print t[:length]
			p t[:spi_data]
		}
	end
	def content_end(c)
		if c[:buscontrol][:content].size >= 2 or (c[:data_write][:dummy] == false and c[:data_read][:dummy] == false)
			content_organize(c[:address], 0xffff)
			if $dma_puts
				c[:address][:content].each{|t|
					dump("address>", t[:spi_data][:value])
				}
			end
			content_organize(c[:data_write], 0x00ff)
			#content_organize(c[:buscontrol], 0x00ff), fix の連結対応ができていないので保留
			dest = []
			prev = {:spi_data => {:dummy => nil}, :flash_command => nil}
			c[:data_read][:content].each{|t|
				if prev[:spi_data][:dummy] != t[:spi_data][:dummy]
					dest << t.dup
					dest[-1][:length] = 0
				end
				dest[-1][:length] += t[:length]
				prev = t
			}
			c[:data_read][:content] = dest
		end
		packet_data = []
		pack = ""
		descriptor_count = []
		if $dma_puts
			content_print(c[:address])
		end
		if c[:address][:content].size != 0
			c[:address][:content][-1][:endmark] = true
		end

		[:buscontrol, :data_write, :data_read, :address].each{|key|
			if c[key][:dummy] == true
				next
			end
			descriptor_count << 0
			content_data = []
			c[key][:content].each{|t|
				content_data << (0x40 | (t[:content_type] << 4) | (t[:spi_data][:type] << 2) | (t[:flash_command] & 0b11))
				if t[:endmark]
					content_data[-1] |= 0x80
				end
				#1 byte での length 表記を一時的に停止. 
				content_data << t[:length]
				pack += "C1v1"
				if key != :data_read
					content_data += t[:spi_data][:value]
					pack += sprintf("%s%d", t[:data_pack_prefix], t[:spi_data][:value].size)
				end
				descriptor_count[-1] += key == :address ? 2 : 1
			}
			packet_data += content_data
		}
		str = 'descriptor counts: '
		sum = 0
		descriptor_count.each{|t|
			str += t.to_s + '+'
			sum += t
		}
		str.chop!
		str += ' -> '
		str += sum.to_s
		#puts str
		r = packet_data.pack(pack).unpack('C*')
		if r.size == 0x40
			#***kuldge***
			r << 0x80
		end
		r
	end
	def header_receive(dodump = false, retry_count = 2)
		data = nil
		retry_count.times{
			data = serialport_receive(1)
			if data != []
				break
			end
			p data
		}
		if (data[0] & 0xe0) != 0x40
			data += serialport_receive((10 + 2) * 4 - 1)
			dump('<bad header ', data)
			puts "header index error:(data[0] & 0xe0) != 0x40"
			return nil
		end

		data.concat(serialport_receive((data[0] & 0x1f) - 1))
		if dodump == true
			dump('<', data)
		end
		sum = 0
		data.each{|t|
			sum += t
		}
		if (sum & 0xff) != 0
			puts "header checksum error:(sum & 0xff) != 0"
			dump('<bad header ', data)
			return nil
		end
		h = {}
		h[:header_length] = data[0]
		h[:command_number] = data[1]
		h[:pc_to_mcu] = le16_unpack(data[2,2])
		h[:mcu_to_pc] = le16_unpack(data[4,2])
		h[:increment] = le16_unpack(data[6,2])
		h[:status] = data[8]
		h[:status_error] = (data[8] & STATUS_ERROR) != 0
		
		if h[:status_error]
			printf "status error line:%d\n", h[:increment]
			str = ['COMMAND', 'DATA'][(h[:status] >> 4) & 1] + ' '
			str += ['READY', 'REQUEST', 'BUSY', 'SEND', 'DONE', 'ABORT'][h[:status] & 0x7]
			puts str
			Fiber.raise
		end
		#p h
		h
	end
	def header_send(number, arg, pc_length, mcu_length, dodump = false)
		d = Array.new(9, 0)
		d[0] = 0x4a
		d[1] = number
		d[2, 2] = le16_pack pc_length
		d[4, 2] = le16_pack mcu_length
		d[6, 2] = arg
		d[6] |= 0x80
		d[8] = STATUS_TYPE_COMMAND | STATUS_REQUEST
		
		checksum_set(d)
		if dodump
			dump('>', d)
		end
		serialport_send(d)
		if number == NUMBER_EXIT
			return
		end
		h = nil
		loop{
			h = header_receive(dodump)
			if h[:command_number] == number
				break
			elsif h[:command_number] == NUMBER_FLASH_STATUS_NOTIFY
				flash_status = serialport_receive(h[:mcu_to_pc])
				printf "send@$%02x>$%02x [$%02x, $%02x] %d\n", number, h[:command_number], flash_status[0], flash_status[1], h[:increment]
			else
				puts "unknown command number"
				xxx
			end
		}
		h
	end

	def content_send_receive(c, command_number, command_arg, dodump = false, progress = -1)
		content = content_end(c)
		
		h = header_send command_number, command_arg, content.size, 0
		if h == nil
			return
		end
		if dodump
			p h
			dump('command 0x%02x>' % command_number, content)
		end
		serialport_send(content)
		h = header_receive
		#p h
		if h[:mcu_to_pc] == 0
			return
		end
		progressbar_draw_index_set(progress)
		data = serialport_receive(h[:mcu_to_pc])
		progressbar_draw_index_set(-1)
		#printf "data.size:%d\n", data.size
		h = header_receive
		if dodump
			p h
		end
		data
	end
	def read(region, length, address, dodump = false, progress = -1)
		c = content_new
		content_access_set(c, length, region, RW_READ, address)
		content_send_receive(c, NUMBER_BUS_ACCESS_START, [0, 0], dodump, progress)
	end

	def write(region, length, address, data, connect_flash_programming = 0, dodump = false)
		c = content_new
		content_access_set(c, length, region, RW_WRITE_WITH_DATA, address, data)
		content_send_receive(c, NUMBER_BUS_ACCESS_START, [connect_flash_programming, 0], dodump)
	end

=begin
		autoselect = []
		autoselect << {:a => c_5555, :d => 0xaa}
		autoselect << {:a => c_2aaa, :d => 0x55}
		autoselect << {:a => c_5555, :d => 0x90}
		id = []
		id << {:a => 0x8000, :d => 0}
		id << {:a => 0x8001, :d => 0}
		reset = [{:a=> 0x8000, :d => 0xf0}]
		flash_id_read(REGION_CPU_6502, autoselect, id, reset)
=end
	def flash_id_read(region_read, region_write, c_0000, c_2aaa, c_5555, dodump = false)
		c = content_new
		#autoselect
		address = {:type => TYPE_RANDOM, :value => [c_5555, c_2aaa, c_5555]}
		data = {:type => TYPE_RANDOM, :value => [0xaa, 0x55, 0x90]}
		content_access_set(c, address[:value].size, region_write, RW_WRITE_WITH_DATA, address, data)
		
		#ID
		address[:value] = [0x8000]
		address[:type] = TYPE_SEQUENTIAL
		content_access_set(c, 2, region_read, RW_READ, address)

		#reset
		data[:value] = [0xf0]
		content_access_set(c, 1, region_write, RW_WRITE_WITH_DATA, address, data)

		content_send_receive(c, NUMBER_BUS_ACCESS_START, [0, 0], dodump)
	end

	def flash_program_program(params)
		c = content_new
		flag = [0, 0]
		params.delete_if{|t|
			t.done?
		}
		#region 0: program -> region 1: program
		params.each{|t|
			t.program_content_make_write(c)
		}
		#region 0: polling read -> region 1: polling read
		params.each{|t|
			t.program_content_make_read(c, flag)
		}
		content_send_receive(c, NUMBER_BUS_ACCESS_PROGRAM, flag)
	end

	def flash_fifo_status_get(dodump = false)
		h = self.header_send NUMBER_FLASH_STATUS_GET, [0, 0], 0, 2, dodump
		if h[:command_number] != NUMBER_FLASH_STATUS_GET
			xxx
		end
		serialport_receive(h[:mcu_to_pc])
	end

	def fifo_notify_get(dodump = false)
		h = nil
		loop{
			h = header_receive(dodump, 2)
			if h[:command_number] == NUMBER_FLASH_STATUS_NOTIFY
				break
			end
			if h[:command_number] != NUMBER_FLASH_STATUS_NOTIFY
				puts("command unmatch")
				h = header_receive(dodump, 2)
				#p h
				#xxx
			end
		}
		r = serialport_receive(h[:mcu_to_pc])
		printf "push [$%02x, $%02x]\n", r[0], r[1]
		r
	end
end
