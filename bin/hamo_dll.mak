	TARGET = hamo.dll
OBJDIR = obj_hamo_dll
include base.mk
CFLAGS += -O2 -D_HAMO_BUILD_
CFLAGS += -I$(MRUBY_DIR)/include -I../include
OBJ = $(addprefix $(OBJDIR)/, hamo.o serialport_windows.o progressbar.o textencoding_windows.o textarea.o)
OBJ += $(addprefix $(OBJDIR)/mruby_, loader.o serialport.o simulator.o)

.PHONY: all clean

all: $(OBJDIR)
	make -C $(SBP_DIR)/busniki/simulator
	make -f hamo_dll.mak $(TARGET)
clean:
	rm -rf $(TARGET) $(OBJDIR)
$(TARGET): $(OBJ) $(SBP_LIB)
	$(CC) -shared -o $@ $^ $(MRUBY_LIB) -lws2_32 -lz
include objdir.mk
