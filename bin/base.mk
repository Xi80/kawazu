CC = clang

SRCDIR = ../source_core
MRUBY_DIR = 
MRUBY_LIB = 
SBP_DIR = 
SBP_LIB = 
-include path.mk

CFLAGS = -Wall -Werror -std=c99 -ferror-limit=2
CFLAGS += -MD -MP -MT $(OBJDIR)/$(*F).o -MF $(OBJDIR)/$(@F).d
