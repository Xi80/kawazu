class DriverTxROM < DriverBase
	def name
		"TxROM, MMC3 and generic assignments"
	end
	def mapper_number
		4
	end
	def bank_assignment
		s = <<EOS
PRGROM, rrr_rrrA_AAAA_AAAA_AAAA, 0x8000, 0xa000
PRGROM, 111_11AA_AAAA_AAAA_AAAA, 0xc000
CHRROM,  rr_rrrr_rAAA_AAAA_AAAA, 0x0000, 0x0800
CHRROM,  rr_rrrr_rrAA_AAAA_AAAA, 0x1000, 0x1400, 0x1800, 0x1c00
EOS
		s += BANK_ASSIGN_GENERIC_WRAM
		s += BANK_ASSIGN_GENERIC_CHRRAM
		s
	end
	def initialize
		super
		@bank_switch_cpu = {0x8000 => 6, 0xa000 => 7}
		@bank_switch_ppu = {0x0000 => 0, 0x0800 => 1, 0x1000 => 2, 0x1400 =>3, 0x1800 => 4, 0x1c00 => 5}
	end
	def nametable_detection
		d = []
		2.times{|nametable_register|
			Memory.write(Command::REGION_CPU_6502, 0xa000, [nametable_register])
			d += ppu_vram_assignment.map
		}
		d[0, 0x10] == NAMETABLE_SYSTEMRAM_A10 &&
		d[0, 0x11] == NAMETABLE_SYSTEMRAM_A11
	end
	def workram_control_set(enable)
		d = 0x40
		if enable
			d = 0x80
		end
		Memory.write(Command::REGION_CPU_6502, 0xa001, [d])
	end
	def irq_disable
		Memory.write(Command::REGION_CPU_6502, 0xe000, [0])
	end
	def bank_switch_cpu(c, region, processor_address, abs_address)
		Memory.content_write(c, Command::REGION_CPU_6502, 0x8000, [@bank_switch_cpu[processor_address], abs_address >> 13], 0x8001)
	end
	def bank_switch_ppu(c, region, processor_address, abs_address)
		#note: If bank size is 0x800 bytes, register data bit 0 is not used.
		Memory.content_write(c, Command::REGION_CPU_6502, 0x8000, [@bank_switch_ppu[processor_address], abs_address >> 10], 0x8001)
	end
end
class DriverTLSROM < DriverTxROM
	def name
		"T[LK]SROM, MMC3 and VRAM A10 = CHARA ROM A17"
		#Character ROM A17 は MMC3 から配線されているかは未調査のため不明
	end
	def mapper_number
		118
	end
	def nametable_detection
		v = []
		6.times{|i|
			v << i
			v << ((i & 1) << 7)
		}
		Memory.write(Command::REGION_CPU_6502, 0x8000, v)
		if ppu_vram_assignment != [2, 2, 3, 3, 2, 3, 2, 3, 0, 0, 1, 1, 0, 1, 0, 1]
			return false
		end
		Memory.write(Command::REGION_CPU_6502, 0x8000, [0x80])
		return ppu_vram_assignment == [2, 3, 2, 3, 2, 2, 3, 3, 0, 1, 0, 1, 0, 0, 1, 1]
	end
end
