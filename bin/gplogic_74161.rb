def cnrom_dump(destfilename, portname = COMPORT_DEFAULT)
	if sbp_open(portname) == false
		return
	end
	cpurom = []
	memory_read(REGION_CPU_6502, cpurom, 0x8000, 0x4000, false)
	memory_read(REGION_CPU_6502, cpurom, 0xc000, 0x4000, false)
	ppurom = []
	4.times{|key|
		rom = []
		4.times{|page|
			val = (key << 4) | page
			0xc000.upto(0xffff){|addr|
				if (cpurom[addr + 0x4000 - 0xc000] & 0x33) != val
					next
				end
				#printf "%04x->%02x\n", addr, val
				command_write(REGION_CPU_6502, 1, {:type => TYPE_FIXED, :value => [addr]}, nil)
				memory_read(REGION_PPU, rom, 0x0000, 0x2000, false)
				break
			}
		}
		ppurom << {:crc => crc32_calc(rom), :rom => rom, :key => key, :length => 0x2000 * 4, :pagesize => 0x2000}
	}
	sbp_close()
	ppurom.uniq!{|item|
		item[:crc]
	}
	c = {:rom => cpurom, :length => cpurom.size, :pagesize => 0x8000}
	if ppurom.size == 1
		puts "no protection"
		nesfile_save(c, ppurom[0], 3, destfilename)
		return
	end
	puts "diode protection detected"
end

def unrom_dump(destfilename, portname = COMPORT_DEFAULT)
	if sbp_open(portname) == false
		return
	end
	cpurom = {:rom => [], :length => 0x40000, :pagesize => 0x4000}
	fixed = []
	memory_read(REGION_CPU_6502, fixed, 0xc000, cpurom[:pagesize])
	(cpurom[:length] / cpurom[:pagesize] - 1).times{|page|
		0xc000.upto(0xffff){|addr|
			if (fixed[addr - 0xc000] & 0xf) != page
				next
			end
			#printf "%04x->%02x\n", addr, val
			command_write(REGION_CPU_6502, 1, {:type => TYPE_FIXED, :value => [addr]}, nil)
			memory_read(REGION_CPU_6502, cpurom[:rom], 0x8000, cpurom[:pagesize])
			break
		}
	}
	sbp_close()
	cpurom[:rom].concat(fixed)
	nesfile_save(cpurom, {:rom => nil, :pagesize=>0x2000}, 2, destfilename)
end
