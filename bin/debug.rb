ES_BACKGROUND_GRAY = "\x1b[48;2;0;80;96m"
ES_BACKGROUND_RESTORE = "\x1b[49m"
ES_PAIR = ["", ES_BACKGROUND_GRAY]
def dump(str, data)
	i = 0
	pair = 0
	line = 0
	if data.size >= 0x10
		str << "\n"
	end
	data.each{|t|
		case i
		when 0, 4, 8, 12
			str += ES_PAIR[pair]
			pair ^= 1
		end
		
		str += "%02X" % t
		
		case i
		when 3, 7, 11
			if pair == 0
				str += ES_BACKGROUND_RESTORE
			end
			str += '-'
		when 15
			str += ES_BACKGROUND_RESTORE
			str += "\n"
			line += 1
			if line >= 4
				pair ^= 1
				line = 0
			end
		else
			str += ' '
		end
		i += 1
		if i >= 0x10
			i = 0
		end
	}
	puts str + ES_BACKGROUND_RESTORE
end

