require './message.rb'
module Nesfile
extend self
	def memory_mirror_detection(memory)
		length = memory[:data].size
		while length > memory[:pagesize]
			l = length / 2
			if memory[:data][0, l] != memory[:data][l, l]
				break
			end
			length /= 2
		end
		memory[:length] = length
	end

	def header_new
		prg = {
			:data => [], :name => "Program ROM", 
			:length => 0
		}
		chr = {
			:data => [], :name => "Charcter RAM", 
			:length => 0
		}
		wram = {
			:data => [], :name => "Cartridge Work RAM",
			:length => 0
		}
		{
			:cpu_program => prg, :ppu_chara => chr, :cpu_wram => wram,
			:mappernum => 0, :vram_mirror => :vram_mirror_h
		}
	end
	def header_load(header)
		if header[0, 4] != [0x4e, 0x45, 0x53, 0x1a]
			return nil
		end
		h = {}
		h[:cpu_program] = {
			:pagesize => 0x4000, :name => "Program ROM", 
			:length => header[4] * 0x4000
		}
		h[:ppu_chara] = {
			:pagesize => 0x1000, :name => "Character ROM",
			:length => header[5] * 0x2000
		}
		if h[:ppu_chara][:length] == 0
			h[:ppu_chara][:name] = "Character RAM"
		end
		h[:cpu_wram] = {:battery => (header[6] & (1 << 1)) != 0}
		h[:vram_mirror] = (header[6] & 1) == 0 ? :vram_mirror_h : :vram_mirror_v
		h[:mappernum] = header[6] >> 4
		h[:mappernum] |= header[7] & 0xf0
		#TBD: mapper number による pagesize の設定
		h
	end

	def crc_print(memory, page_print)
		if memory[:length] == 0 or memory[:data] == nil
			puts memory[:name]
			return
		end
		Message.printf("%s: 0x%X bytes, CRC32 %08X\n", memory[:name], memory[:length], crc32_calc(memory[:data][0, memory[:length]]))
		if page_print == false
			return
		end
		crc = []
		crc_00 = 0
		crc_ff = crc32_calc(Array.new(memory[:pagesize], 0xff))
		display_count = 0
		0.step(memory[:length] - 1, memory[:pagesize]){|i|
			src = memory[:data][i, memory[:pagesize]]
			c = crc32_calc(src)
			f = [crc_00, crc_ff].include?(c)
			if f == false
				display_count += 1
			end
			crc << {:crc => c, :filled => f}
		}

		if display_count >= 4 && display_count <= 16
			width = 4
		else
			width = [display_count, 8].min
		end
		if width == 0
			Message.puts 'ROM data is filled by 0 or 0xff'
			return
		end
		height = display_count / width
		if display_count % width != 0
			height += 1
		end
		str = Array.new(height, "")
		x = 0; y = 0; i = 0
		filled = []
		crc.each{|t|
			if t[:filled] == false
				str[y] += sprintf("%02X:%04X ", i, t[:crc] >> 16)
				y += 1
			else
				filled << i
			end
			if y >= height
				y = 0
				x += 1
			end
			i += 1
		}
		str.each{|t|
			Message.puts t.strip
		}
		if filled.size == 0
			return
		end
		str = 'filled: '
		prev = nil
		start = nil
		filled.each{|t|
			if prev == nil or start == nil
				start = t
			elsif (prev + 1) != t
				str += sprintf("0x%02x-0x%02x ", start, t)
				start = nil
			end
			prev = t
		}
		if start != nil
			str += sprintf("0x%02x-0x%02x ", start, filled[-1])
		end
		Message.puts str.strip
	end
	def rom_get(h)
		h[:data][0, h[:length]]
	end
	def save(h, destfilename)
		memory_mirror_detection(h[:cpu_program])
		crc_print(h[:cpu_program], true)
		chrrom = h[:ppu_chara][:data] != nil
		if chrrom
			memory_mirror_detection(h[:ppu_chara])
		end
		crc_print(h[:ppu_chara], true)
		
		header = [
			0x4e, 0x45, 0x53, 0x1a, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0
		]
		header[4] = h[:cpu_program][:length] / 0x4000
		if chrrom
			header[5] = h[:ppu_chara][:length] / 0x2000
		end
		header[6] = (h[:mappernum] & 0xf) << 4
		header[7] = h[:mappernum] & 0xf0
		f = File.open(destfilename, 'wb')
		f.write(header.pack('C*'))
		f.write(rom_get(h[:cpu_program]).pack('C*'))
		if chrrom
			f.write(rom_get(h[:ppu_chara]).pack('C*'))
		end
		f.close
	end
	def Load(filename)
		f = File.open(filename, 'rb')
		h = header_load(f.read(0x10).unpack('C*'))
		if h == nil
			f.close
			return nil
		end
		h[:cpu_program][:data] = f.read(h[:cpu_program][:length]).unpack('C*')
		h[:ppu_chara][:data] = f.read(h[:ppu_chara][:length]).unpack('C*')
		f.close
		h
	end
end
def nescrc(arg)
	h = Nesfile.Load(arg.shift)
	Nesfile.crc_print(h[:cpu_program], true)
	Nesfile.crc_print(h[:ppu_chara], true)
end
