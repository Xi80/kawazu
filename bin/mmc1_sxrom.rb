require './nesfile.rb'

def mmc1_init
	address = {:type => TYPE_RANDOM, :value => [0x8000]}
	data = {:type => TYPE_RANDOM, :value => [0x80]}
	command_write(REGION_CPU_6502, 1, address, data)
	mmc1_register_write(0x8000, 0x1f)
end
def mmc1_register_write(address, d)
	address = {:type => TYPE_FIXED, :value => [address]}
	data = {:type => TYPE_RANDOM, :value => []}
	5.times{
		data[:value] << d
		d >>= 1
	}
	command_write(REGION_CPU_6502, data[:value].size, address, data)
end

def mmc1_rom_dump(destfilename)
	mmc1_init
	cpu = {:pagesize => 0x4000, :rom => []}
	(0x40000 / cpu[:pagesize]).times{|i|
		mmc1_register_write(0xe000, i)
		cpu[:rom] += memory_read(REGION_CPU_6502, 0x8000, 0x4000)
	}

	ppu = {:pagesize => 0x1000}
	if ppu_chararam_detection == false
		ppu[:rom] = []
		0.step(0x40000 / 0x1000 - 1, 2){|i|
			mmc1_register_write(0xa000, i)
			mmc1_register_write(0xc000, i+1)
			ppu[:rom] += memory_read(REGION_PPU, 0x0000, 0x2000)
		}
	end

	nesfile_save(cpu, ppu, 1, destfilename)
end

module SxROMProgram
	def content_mmc1_register_set(c, cpu_a, register_d)
		a = []; d = []
		5.times{
			a << cpu_a
			d << register_d
			register_d >>= 1
		}
		content_random_write_set(c, REGION_CPU_6502, a, d, 0)
	end
end
class ProgrammerCpuSxROM < Programmer::ProgrammerCpu
	include SxROMProgram
	def initialize(param, rom)
		param[:c_2aaa] = 0x8000 | (0x2aaa & 0x3fff)
		param[:c_5555] = 0xc000 | (0x5555 & 0x3fff)
		param[:dest_processor_address] = 0x8000
		param[:bank_size] = 0x4000
		super(param, rom)
	end
	def cartridge_init
		memory_write(REGION_CPU_6502, 0x8000, [0x80])
	end
	def ppu_programmer_set(t)
		@ppu_programmer = t
	end
	def command_memorybank_update_program_start
		c = content_new
		v = 0b11111
		@dest_processor_address = 0x8000
		if (@dest_abs_address & (1 << 14)) != 0
			v = 0b11011
			@dest_processor_address = 0xc000
		end
		if @dest_abs_address != 0
			command_flash_program_program([self, @ppu_programmer])
		end
		content_mmc1_register_set(c, 0x8000, v)
		content_mmc1_register_set(c, 0xe000, @dest_abs_address >> 14)
		content_flash_erase(c)
		command_rw(c)
	end
	def rom_read(abs_address, length)
		mmc1_reg_write(0x8000, 0b11111)
		rom = []
		while length > 0
			mmc1_reg_write(0xe000, abs_address >> 14)
			l = [length, 0x4000].min
			rom += command_read(REGION_CPU_6502, l, {:type => TYPE_SEQUENTIAL, :value => [0x8000]})
			abs_address += l
			length -= l
		end
		rom
	end
end
class ProgrammerPpuSxROM < Programmer::ProgrammerPpu
	include SxROMProgram
	def initialize(param, rom)
		param[:c_2aaa] = nil
		param[:c_5555] = 0x1000| (0x5555 & 0x0fff) 
		param[:dest_processor_address] = 0x0000
		param[:bank_size] = 0x1000
		super(param, rom)
	end
	def cpu_programmer_set(t)
		@cpu_programmer = t
	end
	def program_content_make_write(c)
		content_random_write_set(c, @region_write, [@c_5555], [0xaa], FLASH_COMMAND_PROGRAMMING)
		if (@dest_abs_address & 0x7000) != 0x2000
			content_mmc1_register_set(c, 0xa000, 0x2aaa >> 12)
		end
		c_2aaa = 0x0000 | (0x2aaa & 0x0fff)
		content_random_write_set(c, @region_write, 
			[c_2aaa, @c_5555], [0x55, 0xa0], FLASH_COMMAND_PROGRAMMING
		)

		if (@dest_abs_address & 0x7000) != 0x2000
			content_mmc1_register_set(c, 0xa000, @dest_abs_address >> 12)
		end
		if @program_page_size != 1
			program_content_make_page(c)
			return
		end
		content_random_write_set(c, @region_write, 
			[@dest_processor_address], [0xdd], FLASH_COMMAND_PROGRAMMING
		)
	end
	def content_flash_erase(c)
		chip_erase = erase_branch
		if chip_erase == :erase_type_none
			return
		end
		c_5555 = 0x1000 | (0x5555 & 0x0fff)
		c_2aaa = 0x0000 | (0x2aaa & 0x0fff)
		content_mmc1_register_set(c, 0xa000, 0x2aaa >> 12)
		content_random_write_set(c, @region_write, 
			[c_5555, c_2aaa, c_5555, c_5555, c_2aaa], 
			[0xaa, 0x55, 0x80, 0xaa, 0x55],
			FLASH_COMMAND_ERASE
		)
		if (@dest_abs_address & 0x3000) != 0x2000
			content_mmc1_register_set(c, 0xa000, @dest_abs_address >> 12)
		end
		content_random_write_set(c, @region_write, [0x0000], [0x30], FLASH_COMMAND_ERASE)
	end
	def command_memorybank_update_program_start
		if @dest_abs_address == 0
			mmc1_reg_write(0xc000, 0x5555 >> 12)
		else
			command_flash_program_program([@cpu_programmer, self])
		end
		c = content_new
		content_flash_erase(c)
		if (@dest_abs_address & 0x3000) == 0x2000
			content_mmc1_register_set(c, 0xa000, @dest_abs_address >> 12)
		end
		command_rw(c)
	end
	def rom_read(abs_address, length)
		mmc1_reg_write(0x8000, 0b11111)
		rom = []
		while length > 0
			page = abs_address >> 12
			mmc1_reg_write(0xa000, page)
			mmc1_reg_write(0xc000, page+1)
			l = [length, 0x2000].min
			rom += command_read(REGION_PPU, l, {:type => TYPE_SEQUENTIAL, :value => [0x0000]})
			abs_address += l
			length -= l
		end
		rom
	end
end
