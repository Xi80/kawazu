TARGET = hamo_cui.exe
OBJDIR = obj_hamo_cui
include base.mk
CFLAGS += -O2  -D_HAMO_LOAD_
CFLAGS += -I../include
OBJ = $(addprefix $(OBJDIR)/, main.o cui_message.o cui_progressbar.o)


.PHONY: all clean
all: $(OBJDIR) $(TARGET)
clean:
	rm -rf $(TARGET) $(OBJDIR)
$(TARGET): $(OBJ) $(SBP_LIB) hamo.dll
	$(CC) -o $@ $^ $(MRUBY_LIB) ./hamo.dll
include objdir.mk
