require './flashmemory_driver.rb'
REGION = ["CPU_6502", "CPU_FLASH", "PPU", "VRAM"]
RW = ["READ", "WRITE", "WRITE_OPEN"]
TYPE = ["RAMDOM", "SEQUENTIAL", "FIXED"]

def ad_str(ad, format)
	str = TYPE[ad[:type]]
	ad[:value].each{|t|
		#str << (format % t)
		str += sprintf(format, t)
	}
	str
end
def ad_update(ad, d, increment_mask)
	case ad[:type]
	when 0
		d += 1
	when 1
		ad[:value][0] += 1
		ad[:value][0] &= increment_mask
	end
	d
end
def dma_beat(channels, num)
	r = nil
	if channels[num][:src_label][0,3] == "RAM"
		r = channels[num][:src_value][channels[num][:index]]
		if (channels[num][:btctrl] & (1 << 10)) != 0
			channels[num][:index] += 1
		end
	end
	channels[num][:count] -= 1
	if channels[num][:count] != 0
		return r
	end
	if channels[num] != channels[num][:descaddr]
		channels[num] = channels[num][:descaddr]
		return r
	end
	channels[num][:count] = channels[num][:btcnt].dup
	channels[num][:index] = 0
	return r
end
def bus_access_simple(t)
	#DMA simulation
	#skip the setup dummy cycles (= dma_channel_number - 1) for the DR channel
	t[:data_read] = []
	t[:dma_channel].size.times{|ch|
		if t[:dma_channel][ch][:src_label] == "IO"
			(t[:dma_channel].size - 1).times{
				dma_beat(t[:dma_channel], ch)
				t[:data_read] << nil
			}
		end
	}

	case t[:rw]
	when 0 #READ
		log = {:prev => nil, :count => 0, :start => nil}
		while t[:dma_channel][0] != nil
			#a = dma_beat(t[:dma_channel], 0) << 8 #AH
			ah = dma_beat(t[:dma_channel], 0)
			#p ah
			a = ah << 8
			a |= dma_beat(t[:dma_channel], 1) #AL
			if log[:start] == nil
				log[:start] = a
				log[:count] = 1
			elsif (log[:prev] + 1) != a && $dma_puts
				if log[:count] == 1
					printf "a:$%04x ", log[:start]
				else
					printf "a:$%04x+ $%x bytes ", log[:start], log[:count]
				end
				log[:start] = a
				log[:count] = 1
			else
				log[:count] += 1
			end
			d = t[:klass].read(t[:region], a)
			if t[:dma_channel][2][:dest_label] == "RAM"
				t[:data_read] << d
			end
			log[:prev] = a
		end
		#discard last dummy cycle data
		t[:data_read].pop
	when 1 #WRITE with data
		while t[:dma_channel][0] != nil
			dw = dma_beat(t[:dma_channel], 0)
			a = dma_beat(t[:dma_channel], 1) << 8 #AH
			a |= dma_beat(t[:dma_channel], 2) #AL
			if $dma_puts
				printf "*(%04x) = %02x; ", a, dw
			end
			t[:klass].write(t[:region], a, dw)
		end
	when 2 #WRITE without data, ROM outputs data to bank register input
		while t[:dma_channel][0] != nil
			a = dma_beat(t[:dma_channel], 0) << 8 #AH
			a |= dma_beat(t[:dma_channel], 1) #AL
			t[:klass].write(t[:region], a, t[:klass].read(t[:region], a))
		end
	end
end
def cartridge_bus_access(t)
	if t[:simple_mode] && $dma_puts
		printf "%s %s 0x%x bytes\n", REGION[t[:region]], RW[t[:rw]], t[:length]
	end
	dma_channel_number = 0
	t[:dma_channel].each{|c|
		prefix = "#%d " % dma_channel_number
		length = 0
		str = ""
		while c != nil
			length += c[:btcnt]
			str += "cnt:$%x" % c[:btcnt]
			c[:count] = c[:btcnt].dup
			c[:index] = 0
			#str += ", ctrl:$%04x" % c[:btctrl]
			str += ", src:%s" % c[:src_label]
			str += ", dest:%s" % c[:dest_label]
			n = c[:descaddr]
			if n == c
				str += "@"
				length = nil
				break
			end
			c = n
			if c == nil
				str += "/"
			else
				str += " -> "
			end
		end
		if length == nil
			prefix += "infi/"
		else
			prefix += "$%x/ " % length
		end
		if $dma_puts
			puts prefix + str
		end
		dma_channel_number += 1
		
	}
	
	#beat all channels
	if t[:simple_mode]
		bus_access_simple(t)
		return
	end
	
#	if t[:dma_channel][0][:descaddr][:src_value][0] == 0b10_0010
#		p t[:dma_channel][2], t[:dma_channel][3]
#		xx
#	end
	str = ''
	ct_prev = nil
	t[:data_read] = Array.new(4, nil)
	while t[:dma_channel][0] != nil
		region = 3
		rw = 3
		ct = dma_beat(t[:dma_channel], 0)
		case ct
		when 0b11_1110 #CPU 6502 READ
			region = 0; rw = 0
		when 0b10_0110 #CPU 6502 WRITE WITH DATA
			region = 0; rw = 1
		when 0b11_0110 #CPU 6502 WRITE OPEN
			region = 0; rw = 2
		when 0b10_0010 #CPU FLASH WRITE
			region = 1; rw = 1
		when 0b11_1001 #PPU READ
			region = 2; rw = 0
		when 0b10_1001 #PPU WRITE
			region = 2; rw = 1
		when 0b01_1001 #VRAM connection
			region = 3; rw = 0
		end
		
		if ct_prev != ct
			if $dma_puts
				puts str
			end
			if rw == 3
				str = 'DUMMY '
			else
				str = sprintf "%s %s ", REGION[region], RW[rw]
			end
		else
			str += ';'
		end
		ct_prev = ct
		
		dw = dma_beat(t[:dma_channel], 1)
		a = dma_beat(t[:dma_channel], 2) << 8
		a |= dma_beat(t[:dma_channel], 3)
		dr = nil
		
		if t[:dma_channel].size == 5
			dma_beat(t[:dma_channel], 4)
			if rw == 0
				dr = t[:klass].read(region, a)
			end
			t[:data_read] << dr
		end
		case rw
		when 1
			t[:klass].write(region, a, dw)
		when 2
			if dr != nil && region < 2
				t[:klass].write(region, a, dw)
			end
		end
		
		#str += sprintf("A:%04X DW:%02X", a, dw)
		str += sprintf("A:%04X DW:%02X", a, dw)
		if dr != nil
			str += " DR:%02X" % dr
		end
	end
	if $dma_puts && str != ''
		puts str
	end
	#p t[:data_read]
end

class SimMemory
	def address_mask(a)
		a &= @memory.size - 1
	end
	def read(a)
		@memory[address_mask(a)]
	end
	def write(a, d)
	end
	def task
	end
end
class SimRam < SimMemory
	def initialize(size)
		@memory = Array.new(size)
	end
	def write(a, d)
		@memory[address_mask(a)] = d
	end
end
class SimRom < SimMemory
	def initialize(data)
		@memory = data
	end
end
class SimFlash < SimRom
	include ParameterFlash
	def initialize(debug_name, data = nil)
		if data == nil
			data = Array.new(memory_capacity, 0)
		end
		super(data)
		@debug_name = debug_name
		@command = []
		@autoselect = false
		@toggle = 0
		@programming_wait = 0
		@program = {
			:data => nil,
			:status => false, :offset => 0
		}
		@AUTOSELECT_ENTER = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0x90}
		]
		@PROGRAM = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0xa0}
		]
		erase = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0x80}, {:c => 0x5555, :d => 0xaa},
			{:c => 0x2aaa, :d => 0x55}
		]
		@ERASE_SECTOR = erase.dup
		@ERASE_SECTOR << {:c => 0x5555, :d => 0x30}
		@ERASE_CHIP = erase.dup
		@ERASE_CHIP << {:c => nil, :d => 0x10}
		[@AUTOSELECT_ENTER, @PROGRAM, @ERASE_SECTOR, @ERASE_CHIP].each{|a|
		a.each{|b|
			b[:c] &= command_address_mask
		}}
	end
	def cmp(a, b)
		if a.size != b.size
			return false
		end
		a.size.times{|i|
			if a[i][:c] != b[i][:c] || a[i][:d] != b[i][:d]
				return false
			end
		}
		true
	end
	def address_dump(t)
		str = ''
		t.each{|s|
			str += ("0x%05X " % s[:a])
		}
		str
	end
#	def write(a, d)
#	end
	def read(a)
		if @autoselect
			return read_autoselect(a)
		elsif @program[:status] #bit 6 のみサポート
			r = @toggle
			@toggle ^= 1 << 6
			@programming_wait -= 1
			if @programming_wait <= 0
				@program[:status] = false
				@memory[@program[:offset], @program[:data].size] = @program[:data]
				if $dma_puts
					#p @memory[@program[:offset]], @program[:data].size, @program[:data][0]
					puts(@debug_name + "flash toggle done")
				end
			end
			return r
		end
		super(a)
	end
	def read_autoselect(a)
		case a & autoselect_address_mask
		when 0
			return manufacturers_id
		when 1
			return device_id
		else #sector protect verify は未サポート
			return 0xff
		end
	end
end
class SimAM29F040B < SimFlash
	include ParameterAM29F040B
	def write(a, d)
		@command << {:a => address_mask(a), :c => a & command_address_mask, :d => d}
		if @command.size > 6
			@command.shift
		end
		if (
			@command.size >= 6 && !cmp(@command[-4, 3], @PROGRAM) && (
				((a & 0x03ff) == 0x02aa && (a & 0x3fff) != 0x2aaa) || 
				((a & 0x03ff) == 0x0555 && (a & 0x3fff) != 0x5555)
			)
		)
			printf "%s ?%x ", @debug_name, a
			xxx
		end

		if cmp(@command, @ERASE_CHIP)
			@program[:offset] = 0
			@program[:data] = Array.new(memory_capacity , 0xff)
			@programming_wait = 50 * 2;
			@program[:status] = true
			puts @debug_name + address_dump(@command) +  "chip erase"
			return
		elsif cmp(@command[0, 5], @ERASE_SECTOR[0, 5]) && @ERASE_SECTOR[-1][:d] == d #&& false
			@program[:offset] = address_mask(a) & 0xf_0000
			@program[:data] = Array.new(0x10000 , 0xff)
			@programming_wait = 10 * 2
			@program[:status] = true
			puts @debug_name + address_dump(@command) +  "sector erase"
			return
		end
		if @command.size >= 4 && cmp(@command[-4, 3], @PROGRAM)
			@program[:status] = true
			@program[:offset] = address_mask(a)
			@program[:data] = [d]
			@programming_wait = 4
			if $dma_puts
				puts @debug_name + address_dump(@command[-4, 4]) + "program"
			end
		elsif d == 0xf0
			@autoselect = false
			puts @debug_name + "autoselect off"
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_ENTER)
			@autoselect = true
			puts address_dump(@debug_name + @command[-3, 3]) + "autoselect on"
		end
	end
end
class SimSST39SF040 < SimAM29F040B
	include ParameterSST39SF040
end
=begin
During the byte-load cycle, the addresses are latched by the falling edge of either #CE or #WE, whichever occurs last. The data are latched by the rising edge of either #CE or #WE, whichever occurs first. 

If the host loads a second byte into the page buffer within a byte-load cycle time (TBLC) of 200 uS after the initial byte-load cycle, the W29C040 will stay in the page load cycle. 
Additional bytes can then be loaded consecutively. The page load cycle will be terminated and the internal write (erase/program) cycle will start if no additional byte is loaded into the page buffer. 
A8 to A18 specify the page address. All bytes that are loaded into the page buffer must have the same page address. A0 to A7 specify the byte address within the page. The bytes may be loaded in any order; sequential loading is not required.
=end
class SimW29C040 < SimFlash
	include ParameterW29C040
	def initialize(debug_name, data)
		super(debug_name, data)
		@AUTOSELECT_EXIT = [
			{:c => 0x5555, :d => 0xaa}, {:c => 0x2aaa, :d => 0x55},
			{:c => 0x5555, :d => 0xf0}
		]
		@page_programming = 0
	end
	def write(a, d)
		if @page_programming != 0
			if @program[:data].size == 0
				@program[:offset] = address_mask(a)
			elsif (@program[:offset] & 0xffff_ff00) != (a & 0xffff_ff00)
				puts "programming offset is not much"
				Fiber.raise
			end
			@page_programming -= 1
			@program[:data] << d
			if @page_programming == 0
				@program[:status] = true
				@programming_wait = 40 * 2
				if $dma_puts
					puts "page programming is accepted@$%06x" % @program[:offset]
				end
			end
			return
		end
		@command << {:a => address_mask(a), :c => a & 0x7fff, :d => d}
		if @command.size > 6
			@command.shift
		end
		#chip erase のみ, sector erase なし
		if cmp(@command, @ERASE_CHIP)
			@program[:offset] = 0
			@program[:data] = Array.new(memory_capacity , 0xff)
			@programming_wait = 50 * 2;
			@program[:status] = true
			puts address_dump(@command) +  "erase"
			return
		end
		#page programming, 0x100 bytes
		if @command.size >= 3 && cmp(@command[-3, 3], @PROGRAM)
			@page_programming = 0x100
			@program[:data] = []
			if $dma_puts
				puts address_dump(@command[-3, 3]) + "page program start"
			end
			return
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_ENTER)
			@autoselect = true
			puts address_dump(@command[-3, 3]) + "autoselect on"
		end
		if @command.size >= 3 && cmp(@command[-3, 3], @AUTOSELECT_EXIT)
			@autoselect = false
			puts address_dump(@command[-3, 3]) + "autoselect off"
		end
	end
end

def sim_flash_get(device_name, debug_name, memory_data = nil)
	r = nil
	case device_name
	when "AM29F040B"
		r = SimAM29F040B.new(debug_name, memory_data)
	when "W29C040"
		r = SimW29C040.new(debug_name, memory_data)
	when "SST39SF040"
		r = SimSST39SF040.new(debug_name, memory_data)
	end
	r
end

class Cartridge
	def initialize
		@prg_rom = nil
		@chrmem = nil
	end
end

class PCB_SxROM < Cartridge
	def initialize
		@shift_count = nil
		@mmc_register = Array.new(4, 0b11111)
		@prg_ram = nil
		@shift_register = 0
	end
	def prg_ram_enabled
		(@mmc_register[3] & 0x10) == 0
	end
	def prg_rom_address(a)
		a14 = a & (1 << 14)
		case (@mmc_register[0] >> 2) & 0b11
		when 0, 1 #switch 32 KB at $8000, ignoring low bit of bank number
			a = nil #TBD
		when 2
			a &= 0x3fff
			if a14 == 0
				a |= 0 << 14
			else
				a |= @mmc_register[3] << 14
			end
		when 3
			a &= 0x3fff
			if a14 == 0
				a |= @mmc_register[3] << 14
			else
				a |= 0b11111 << 14
			end
		end
		a
	end
	def chrmem_address(a)
		register_data = @mmc_register[1 + ((a >> 12) & 1)]
		a &= 0x0fff
		a |= register_data << 12
		a
	end
	def write(region, a, d)
		case region
		when 0
			if a >= 0x8000 && a <= 0xffff && (d & 0x80) != 0
				@shift_count = -1
			end
			case a
			when 0x6000..0x7fff
				if prg_ram_enabled
					@prg_ram.write(a & 0x1fff, d)
				end
			when 0x8000..0xffff
				@shift_register >>= 1
				@shift_register |= (d&1) << 4
				@shift_count += 1
				if @shift_count >= 5
					@shift_count = 0
					register_address = a >> 13
					register_address &= 0b11
					@mmc_register[register_address] = @shift_register
				end
			end
		when 1
			@prg_rom.write(prg_rom_address(a), d)
		when 2
			case a
			when 0x0000..0x1fff
				@chrmem.write(chrmem_address(a), d)
			end
		end
	end
	def read(region, a)
		case region
		when 0
			case(a)
			when 0x6000..0x7fff
				if prg_ram_enabled
					return @prg_ram.read(a & 0x1fff)
				end
			when 0x8000..0xffff
				return @prg_rom.read(prg_rom_address(a))
			else
				return nil
			end
		when 2
			case(a)
			when 0x0000..0x1fff
				return @chrmem.read(chrmem_address(a))
			end
		end
		return nil
	end
end
class PCB_SKROM < PCB_SxROM
	def initialize(prg_rom, chrrom)
		super()
		@prg_ram = SimRam.new(0x2000)
		@prg_rom = prg_rom
		@chrmem = chrrom
	end
end

class PCB_TxROM < Cartridge
	def initialize(chara_reg_initial_value)
		@page_register = Array.new(8, 0)
		@page_address = nil
		@gp_register = Array.new(8, nil)
		if chara_reg_initial_value == nil
			return
		end
		6.times{|i|
			@gp_register[i] = chara_reg_initial_value
		}
	end
	def prg_ram_enabled
		(@gp_register[3] & 0x80) != 0
	end
	def prg_ram_writable
		(@gp_register[3] & 0x40) == 0
	end
	def prg_rom_address(a)
		a14_13 = (a >> 13) & 0b11
		v = nil
		case a14_13 ^ ((((@gp_register[0] >> 6) & 1)) << 1)
		when 0
			v = @page_register[6]
		when 1
			v = @page_register[7]
		when 2
			v = 0x3e
		when 3
			v = 0x3f
		end
		a &= 0x1fff
		a |= v << 13
		a
	end
	def chrmem_address(a)
		a12_a10 = (a >> 10) & 0b111
		a12_a10 ^= (((@gp_register[0] >> 7) & 1)) << 2
		case a12_a10
		when 0..3
			v = @page_register[a12_a10 >> 1]
			v &= 0xfe
			v |= a12_a10 & 1
		when 4..7
			v = @page_register[2 + (a12_a10 & 0b11)]
		else
			xxx
		end
		a &= 0x03ff
		a |= v << 10
		a
	end
	def write(region, a, d)
		case region
		when 0
			case a
			when 0x6000..0x7fff
				if prg_ram_enabled && prg_ram_writable
					@prg_ram.write(a & 0x1fff, d)
				end
			when 0x8000..0xffff
				register_address = a & 1
				register_address |= ((a >> 13) & 0b11) << 1
				@gp_register[register_address] = d
				if register_address == 1
					@page_register[@gp_register[0] & 0b111] = d
				end
			end
		when 1
			@prg_rom.write(prg_rom_address(a), d)
		when 2
			case a
			when 0x0000..0x1fff
				@chrmem.write(chrmem_address(a), d)
			end
		end
	end
	def read(region, a)
		case region
		when 0
			case(a)
			when 0x6000..0x7fff
				if prg_ram_enabled
					return @prg_ram.read(a & 0x1fff)
				end
			when 0x8000..0xffff
				return @prg_rom.read(prg_rom_address(a))
			else
				return nil
			end
		when 2
			case(a)
			when 0x0000..0x1fff
				return @chrmem.read(chrmem_address(a))
			end
		end
		return nil
	end
end
class PCB_TNROM < PCB_TxROM
	def initialize(prg_rom, chara_reg_initial_value = nil)
		super(chara_reg_initial_value)
		@prg_ram = SimRam.new(0x2000)
		@prg_rom = prg_rom
		@chrmem = SimRam.new(0x2000)
	end
	def chrmem_address(a) #character RAM address bus is not used MMC3 banks
		a & 0x1fff
	end
end
class PCB_TKROM < PCB_TxROM
	def initialize(prg_rom, chr_rom, chara_reg_initial_value = nil)
		super(chara_reg_initial_value)
		@prg_ram = SimRam.new(0x2000)
		@prg_rom = prg_rom
		@chrmem = chr_rom
	end
end
