def serialport_receive(length)
	i = 0
	retry_count = 4000
	receive_data = serialport_receive_buffer_get
	size_prev = receive_data.size
	while receive_data.size < length && i < retry_count
		Fiber.yield
		if size_prev == receive_data.size
			i += 1
		else
			i = 0
		end
		size_prev = receive_data.size
	end
	if receive_data.size < length
		return []
	end
	receive_data.slice!(0, length)
end
def usleep(us)
	(us/1000).times{|t|
		Fiber.yield
	}
end
