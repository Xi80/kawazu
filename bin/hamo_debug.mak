TARGET = hamo_debug.exe
OBJDIR = obj_debug
include base.mk
CFLAGS += -O0 -g -D_HAMO_STATICLINK_
CFLAGS += -I$(MRUBY_DIR)/include -I../include
OBJ = $(addprefix $(OBJDIR)/, main.o hamo.o serialport_windows.o progressbar.o cui_progressbar.o textencoding_windows.o textarea.o cui_message.o)
OBJ += $(addprefix $(OBJDIR)/mruby_, loader.o serialport.o simulator.o)

.PHONY: all clean

all: $(OBJDIR)
	make -C $(SBP_DIR)/busniki/simulator
	make -f hamo_debug.mak $(TARGET)
clean:
	rm -rf $(TARGET) $(OBJDIR)
$(TARGET): $(OBJ) $(SBP_LIB)
	$(CC) -o $@ $^ $(MRUBY_LIB) -lws2_32 -lz
include objdir.mk
