#デバッグ用でコンパイルはしない.
#naruko がコマンドをよく忘れるので短縮コマンドとして利用している.
.PHONY: sim_dump sim_program sbp_dump sbp_program
HAMO = ./hamo_debug.exe
sim_dump:
	$(HAMO) -s -f simulator/simulator.rb sim_dump d:/work/hvc/unagi/nes/shi-3g.nes
sim_program:
	$(HAMO) -s -f simulator/simulator.rb sim_program d:/work/hvc/unagi/nes/shi-3g.nes
sbp_dump:
	$(HAMO) -f sbp_test.rb sbp_mmc3_rom_dump e:/hoge.nes
sbp_program:
	$(HAMO) -f sbp_test.rb sbp_flash_program d:/work/hvc/unagi/nes/shi-3g.nes
gdb:
	gdb -x sim.gdb $(HAMO)

