module Memory
extend self
	def read(region, address, length, progress = -1)
		Command.read(region, length, {:type => Command::TYPE_SEQUENTIAL, :value=>[address]}, false, progress)
	end
	def ad_set(address, data, address_and, address_add)
		a = {:type => Command::TYPE_RANDOM, :value => [address]}
		if address_add == 0 && address_and == 0xffff
			a[:type] = Command::TYPE_FIXED
		elsif address_add == 1 && address_and == 0xffff
			a[:type] = Command::TYPE_SEQUENTIAL
		else
			a[:value] = []
			data.size.times{
				a[:value] << address
				address += address_add
				address &= address_and
			}
		end
		d = {:type => Command::TYPE_RANDOM, :value => data}
		return a, d
	end
	def write(region, address, data, address_and = 0xffff, address_add = 1)
		a, d = ad_set(address, data, address_and, address_add)
		Command.write(region, d[:value].size, a, d)
	end
	def content_write(c, region, address, data, address_and = 0xffff, address_add = 1)
		a, d = ad_set(address, data, address_and, address_add)
		Command.content_access_set(c, d[:value].size, region, Command::RW_WRITE_WITH_DATA, a, d)
	end
	def chararam_detection
		address = 0x1234
		data = [0, 0xff]
		7.times{|i|
			data << (1 << i)
		}
		write(Command::REGION_PPU, address, data)
		read(Command::REGION_PPU, address, data.size) == data
	end
	def vram_assignment
		a = {:type => Command::TYPE_RANDOM, :value=>[]}
		0.step(0x4000 - 1, 1 << 10){|aa|
			a[:value] << aa
		}
		Command.read(Command::REGION_VRAM_CONNECTION, a[:value].size, a).map{|t|
			t &= 0b11
			t ^= 0b11
			t
		}
	end
end
