module ParameterFlash
	def parameter_get(s)
		s[:erase_supported] = [:erase_type_entire]
	end
	def memory_capacity
		0x80
	end
end
class DriverFlash
	include ParameterFlash
end
module ParameterAM29F040B
	def name
		"AM29F040B"
	end
	def memory_capacity
		0x80000
	end
	def command_address_mask
		0x007ff
	end
	def autoselect_address_mask
		0xff #2桁なので A7:0 (?)
	end
	def manufacturers_id
		0x01
	end
	def device_id
		0xa4
	end
	def equable_sector(s, size)
		s[:erase_sector] = []
		0.step(memory_capacity - 1, size){|i|
			s[:erase_sector] << (i...(i+size))
		}
	end
	def parameter_get(s)
		super(s)
		s[:skip_data_0xff] = true
		#s[:skip_data_0xff] = false
		s[:program_page_size] = 1
		s[:erase_supported] << :erase_type_sector
		equable_sector(s, 0x10000)
	end
end
class DriverAM29F040B < DriverFlash
	include ParameterAM29F040B
end
module ParameterSST39SF040
	def name
		"SST39SF040"
	end
	def command_address_mask
		0x07fff
	end
	def autoselect_address_mask
		0xffff #range 詳細不明 A15:0??
	end
	def manufacturers_id
		0xbf
	end
	def device_id
		0xb7
	end
	def parameter_get(s)
		super(s)
		#sector address range A11:0
		#todo:sector size が banks size より小さい場合の対応
		equable_sector(s, 0x1000)
	end
end
class DriverSST39SF040 < DriverAM29F040B
	include ParameterSST39SF040
end
module ParameterMX29F040C
	def name
		"MX29F040C"
	end
	#command address mask 3桁ではあるが詳細の明記なし.
	def manufacturers_id
		0xc2
	end
	def device_id
		0xa4
	end
end
module ParameterW29C040
	def name
		"W29C040"
	end
	def command_address_mask
		0x07fff
	end
	def autoselect_address_mask
		0x7ffff #A18:1
	end
	def manufacturers_id
		0xda
	end
	def device_id
		0x46
	end
	def memory_capacity
		0x80000
	end
	def parameter_get(s)
		super(s)
		s[:skip_data_0xff] = false
		s[:program_page_size] = 0x100
	end
end
class DriverW29C040 < DriverFlash
	include ParameterW29C040
end

def sbp_flash_get(name)
	r = nil
	case name
	when "AM29F040B"
		r = DriverAM29F040B.new
	when "W29C040"
		r = DriverW29C040.new
	when "SST39SF040"
		r = DriverSST39SF040.new
	end
	r
end
