require './programmer.rb'
class DriverBase
	NAMETABLE_SYSTEMRAM_A10 = [
		2, 3, 2, 3, 2, 3, 2, 3, 0, 1, 0, 1, 0, 1, 0, 1
	]
	NAMETABLE_SYSTEMRAM_A11 = [
		2, 2, 3, 3, 2, 2, 3, 3, 0, 0, 1, 1, 0, 0, 1, 1
	]
	BANK_ASSIGN_GENERIC_WRAM = 
		"WRAM,          A_AAAA_AAAA_AAAA, 0x6000\n"
	BANK_ASSIGN_GENERIC_CHRRAM = 
		"CHRRAM,        A_AAAA_AAAA_AAAA, 0x0000\n"
	MEMORY_TO_REGION = {
		"PRGROM" => Command::REGION_CPU_6502,
		"WRAM" => Command::REGION_CPU_6502,
		"CHRROM" => Command::REGION_PPU,
		"CHRRAM" => Command::REGION_PPU
	}
	def initialize
		@bank_list = {}
		bank_define bank_assignment
	end
private
	def bank_define(str)
		str.split("\n").each{|t|
			s = t.split(",")
			memory = s.shift
			h = {}
			h[:assign_processor] = 0
			h[:assign_register] = 0
			h[:assign_0] = 0
			h[:assign_1] = 0
			h[:width] = 1
			str = s.shift.strip.gsub('_', '')
			str.split('').each{|a|
				h[:width] <<= 1
				h[:assign_processor] <<= 1
				h[:assign_register] <<= 1
				h[:assign_0] <<= 1
				h[:assign_1] <<= 1
				case a
				when '0'
					h[:assign_0] |= 1
				when '1'
					h[:assign_1] |= 1
				when 'A'
					h[:assign_processor] |= 1
				when 'r'
					h[:assign_register] |= 1
				else
					xxx
				end
			}
			h[:entry] = []
			temp = []
			s.each{|a|
				temp << a.to_i(0)
			}
			temp.sort.each{|e|
				h[:entry] << (e..(e+h[:assign_processor]))
			}
			if @bank_list.key?(memory) == false
				@bank_list[memory] = []
			end
			@bank_list[memory] << h
		}
	end
=begin
	def memory_read(region, address, length)
		#printf "READ r:%d a:$%04x, l:$%x\n", region, address, length
		Memory.read(region, address, length)
	end
	def memory_write(c, region, address, data, address_and = 0xffff, address_add = 1)
		str = sprintf("WRITE r:%d a:$%04x, l:$%x, d:", region, address, data.size)
		data.each{|d|
			str += "$%02x " % d
		}
		#puts str
		Memory.write_content(c, region, address, data, address_and, address_add)
		#Memory.write(region, address, data, address_and, address_add)
	end
=end
	def rom_dump_memory(memory)
		if @bank_list.key?(memory) == false
			return
		end
		b = @bank_list[memory]
		region = MEMORY_TO_REGION[memory]
		if b.size == 1
			c = b[0]
			if c[:assign_register] != 0 || c[:assign_0] != 0 || c[:assign_1] != 0 || c[:entry].size != 1
				xxx
			end
			#仕様変更のため動かないと予想される. 後日要修正.
			e = c[:entry][0]
			memory_read(memory, e.begin, e.size)
			return
		end
		data = []
		progressbar_index = region == Command::REGION_CPU_6502 ? 0 : 1
		b.each{|t|
			if t[:assign_register] == 0
				next
			end
			length = t[:width]
			switchable_total_length = (t[:assign_processor] + 1) * t[:entry].size
			rom_abs_address = 0
			while length > 0
				c = Command.content_new
				t[:entry].each{|e|
					bank_switch(c, region, e.begin, rom_abs_address)
					rom_abs_address += e.size
				}
				Command.content_send_receive(c, Command::NUMBER_BUS_ACCESS_START, [0, 0])
				l = [switchable_total_length, length].min
				data += Memory.read(region, t[:entry][0].begin, l, progressbar_index)
				length -= l
			end
			break
		}
		data
	end
	def pagesize_get(memory)
		b = @bank_list[memory]
		ar = []
		b.each{|t|
			t[:entry].each{|s|
				ar << s.size
			}
		}
		ar.min
	end
	def rom_dump_main(h, memory)
		#p @bank_list[memory][0][:width]
		h[:pagesize] = pagesize_get(memory)
		h[:data] = rom_dump_memory(memory)
	end
	def workram_control_set(enable)
	end
	def irq_disable
	end
	def programming_address_set(t, e, switch)
		h = {}
		h[:absolute] = t[:assign_0] | t[:assign_1]
		h[:processor] = [e]
		h[:combined_bank] = e
		h[:switch] = switch
		h
	end
	def programming_address_assignment(m, memory)
		bank = @bank_list[memory].dup
		bank.delete_if{|t|
			if (
				(t[:assign_0] & (1 << 14)) != 0 ||
				(t[:assign_1] & (1 << 14)) != 0
			)
				r = t[:entry].shift
				if (t[:assign_0] & (1 << 14)) != 0
					m[:c_2aaa] = programming_address_set(t, r, false)
					m[:c_2aaa][:absolute] |= 0x02aaa #固定値を介するため上位アドレスbitは |= 演算子で設定
				else
					m[:c_5555] = programming_address_set(t, r, false)
					m[:c_5555][:absolute] |= 0x05555
				end
				next t[:entry].size == 0
			end
			false
		}
		bank.sort!{|a,b|
			a[:entry].size <=> b[:entry].size
		}
		bank.delete_if{|t|
			if m[:c_dest] != nil
				break
			end
			while t[:entry].size != 0 && m[:c_dest] == nil
				if m[:c_2aaa] == nil
					r = t[:entry].shift
					m[:c_2aaa] = programming_address_set(t, r, true)
					m[:c_2aaa][:absolute] = 0x02aaa #register を介するのでレジスタアドレスbitはすべて0なので = 演算子で設定
				elsif m[:c_5555] == nil
					r = t[:entry].shift
					m[:c_5555] = programming_address_set(t, r, true)
					m[:c_5555][:absolute] = 0x05555
				elsif m[:c_dest] == nil
					r = t[:entry].shift
					m[:c_dest] = programming_address_set(t, r, true)
				end
			end
			t[:entry].size == 0
		}
		if m[:c_dest] == nil
			puts("can't assign programming addresses")
			xxx
			return
		end
		if m[:c_dest][:combined_bank].size < 0x1000
			bank.each{|t|
				while t[:entry].size != 0
					e = t[:entry].shift
					m[:c_dest][:processor] << e
					m[:c_dest][:combined_bank] = (m[:c_dest][:combined_bank].begin .. e.end)
					if m[:c_dest][:combined_bank].size >= 0x1000
						break
					end
				end
			}
		end
		#p m
	end
	def bank_switch(c, region, processor_address, abs_address)
		case region
		when Command::REGION_CPU_6502, Command::REGION_CPU_FLASH
			bank_switch_cpu(c, region, processor_address, abs_address)
		when Command::REGION_PPU
			bank_switch_ppu(c, region, processor_address, abs_address)
		end
	end
public
	def rom_dump(destfilename)
		h = Nesfile.header_new
		
		chararam = Memory.chararam_detection
		progressbar_range_set(0, "Program ROM", @bank_list["PRGROM"][0][:width])
		if chararam
			progressbar_range_set(1, "Character RAM", 0)
		else
			progressbar_range_set(1, "Character ROM", @bank_list["CHRROM"][0][:width])
		end
		progressbar_ready
		rom_dump_main(h[:cpu_program], "PRGROM")
		if chararam == false
			h[:ppu_chara][:name] = "Character ROM"
			rom_dump_main(h[:ppu_chara], "CHRROM")
		end
		progressbar_done
		h[:mappernum] = self.mapper_number
		Nesfile.save(h, destfilename)
	end
	def flash_program_init_cpu(t, nesfile)
		programming_address_assignment(t[:parameter], "PRGROM")
		t[:programmer] = Programmer::ProgrammerCpu.new(self, t[:parameter], nesfile[:cpu_program][:data])
	end
	def flash_program_init_ppu(t, nesfile)
		programming_address_assignment(t[:parameter], "CHRROM")
		t[:programmer] = Programmer::ProgrammerPpu.new(self, t[:parameter], nesfile[:ppu_chara][:data])
	end
end
