require './debug.rb'
def sbp_open(portname)
	if serialport_open(portname) == false
		puts "cannot open " + portname
		return false
	end
	serialport_send [0x20, 0x0d]
	loop{
		r = serialport_receive(1)
		if r == [0x0a]
			break
		end
	}
	serialport_rx_purge
	dummy = Array.new(10, 0x4a)
	serialport_send dummy
	r = serialport_receive(dummy.size)
	if r == dummy
		#text mode
		serialport_send [0x0d]
		serialport_receive(1)
		serialport_rx_purge
		key = "^L?j6&B*JJ\r"
		serialport_send key.unpack('C*')
		key += "\n"
		r = serialport_receive(12)
		#p r.pack('C*')
		while r != key.unpack('C*')
			r = r[1,11]
			t = serialport_receive(1)
			if t == []
				p r
				return false
			end
			r +=  t
		end
		
		r = serialport_receive(10)
		p r
		#dump 'text->binary ', r
	elsif [r[0], r[8]] == [0x4a, 0x18]
		dump 'binary->binary ', r
	else
		puts "device initalize error"
		dump '', r
		return false
	end
	true
end
def sbp_close
	Command.header_send(Command::NUMBER_EXIT, [0, 0], 0, 0)
	serialport_close()
end
