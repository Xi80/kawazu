require './sbp_serialport.rb'

module Bootloader
extend self
	ENTRY = "J@!Pn^y;x\r"
	def program_init
		serialport_send([0x20])
		serialport_rx_purge
		command = ENTRY.unpack('C*')
		serialport_send(command)
		reply = []
		while reply != command
			if reply.size >= command.size
				reply.shift
			end
			r = serialport_receive(1)
			if r == []
				puts "communication error"
				return false
			end
			reply += r
		end
		r = serialport_receive(2)
		if r.pack('C*') != "\n3"
			puts "command reply error:" + r.pack('C*')
			return false
		end
		true
	end
	def program_data(buf, address)
		while buf.size > 0
			l = [buf.size, 0x40].min
			d = [0xa5]
			d.concat([address].pack('V1').unpack('C4'))
			serialport_send(d)
			r = serialport_receive(1)
			if r == []
				puts "address reply timeout"
				return false
			end
			if r[0] != 0x40
				puts "address reply error"
				p r.pack('C*')
				return false
			end
			d = Array.new(0x40, 0xff)
			d[0, l] = buf.slice!(0, l)
			serialport_send(d)
			r = serialport_receive(1)
			if r == nil
				puts "data reply timeout"
				return false
			end
			if r[0] != 0x40
				puts "data reply error"
				return
			end
			address += l
			printf "\r%d", buf.size
			#printf "%x ", address
		end
		true
	end
	def program_main(buf, address)
		if program_init == false
			return
		end
		buf_first = buf[0, 0x100]
		buf[0, 0x100] = Array.new(0x100, 0xdd)
		if program_data(buf, address) == false
			return
		end
		if program_data(buf_first, address) == false
			return
		end

		serialport_send [0xd7]
		puts "programming is done."
	end
end

def firmware_program(arg)
	filename = arg.shift
	address = arg.shift.to_i(0)
	portname = "COM3"
	if arg.size != 0
		portname = arg.shift
	end
	if address < 0x1000 || address >= 0x8000
		puts "programming address range error."
		return
	end
	if File.exist?(filename) == false
		printf "file %s is not found.\n", filename
		return
	end
	f = File.open(filename, 'rb')
	buf = f.read.unpack 'C*'
	f.close
	
	if serialport_open(portname) == false
		puts "cannot open " + portname
		return
	end
	Bootloader.program_main(buf, address)
	serialport_close()
end
