#pragma once
struct progressbar;
struct progressbar_callback;

//called for mruby methods
struct mrb_state;
void mruby_progressbar_method_define(struct mrb_state *m);

//called by serialport drivers
void progressbar_add(int length);
