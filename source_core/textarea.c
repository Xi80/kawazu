#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <hamo.h>
#include "textarea_local.h"
#include "textencoding.h"

struct textarea{
	void *klass;
	const struct textarea_callback *m;
	FILE *log;
};
static struct textarea *textarea_new(void *klass, const struct textarea_callback *callback, const char *logfilename)
{
	struct textarea *t = malloc(sizeof(struct textarea));
	t->klass = klass;
	t->m = callback;
	if(logfilename){
		t->log = FOPEN(logfilename, "w");
	}else{
		t->log = NULL;
	}
	return t;
}
static void textarea_delete(struct textarea *t)
{
	if(t->log){
		fclose(t->log);
	}
	free(t);
}
static void textarea_print(struct textarea *t, const char *str)
{
	if(t->log){
		fputs(str, t->log);
	}
	if(t->m->print){
		t->m->print(t->klass, str);
	}
}

static struct textarea *global_message = NULL;

void message_new(void *klass, const struct textarea_callback *callback, const char *logfilename)
{
	global_message = textarea_new(klass, callback, logfilename);
}
void
message_delete(void)
{
	textarea_delete(global_message);
}

void message_printf(const char *format, ...)
{
	va_list l;
	va_start(l, format);
	size_t n = _vsnprintf(NULL, 0, format, l) + 1;
	char *buf = malloc(n);
	vsnprintf(buf, n, format, l);
	textarea_print(global_message, buf);
	free(buf);
	va_end(l);
}
void message_puts(const char *str)
{
	textarea_print(global_message, str);
	textarea_print(global_message, "\n");
}

#include <mruby.h>
#include <mruby/string.h>
#include "mruby_driver.h"

#define DECLARE_MESSAGE(NAME) struct textarea *const NAME = global_message

static mrb_value m_message_print(mrb_state *m, mrb_value self)
{
	DECLARE_MESSAGE(d);
	assert(d);
	mrb_value str;
	mrb_get_args(m, "S", &str);
	textarea_print(d, RSTRING_PTR(str));
	return mrb_nil_value();
}
static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
	M(message_print, 1),
	{NULL, NULL, 0}
#undef M
};
void mruby_textarea_method_define(struct mrb_state *m)
{
	script_method_define(m, list);
}
