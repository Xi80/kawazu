#include <assert.h>
#include <zlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <mruby.h>
#include <mruby/array.h>
#include <mruby/string.h>
#include <mruby/compile.h>
#include <mruby/error.h>
#include <hamo.h>
#include "mruby_driver.h"
#include "mruby_loader.h"
#include "textencoding.h"
#include "progressbar_local.h"
#include "textarea_local.h"

void script_method_define(mrb_state *m, const struct script_function_list *l)
{
	while(l->name != NULL){
		mrb_define_method(m, m->kernel_module, l->name, l->func, MRB_ARGS_REQ(l->argc));
		l++;
	}
}
static void my_error_print(mrb_state *m)
{
	mrb_print_error(m);
	//m->exc = 0;
//エラー出力の内容が mrb_print_error とは一部欠けていて使い物にならない. mrb_print_error では出力は stderr 固定でログに取り込めない. よって mruby の API を変更して file format も取れるようにする予定.
/*	FILE *const f = FOPEN(LOG_FILENAME_C, "a");
	mrb_value backtrace = mrb_get_backtrace(m);
	const mrb_int n = RARRAY_LEN(backtrace);
	for (mrb_int i = 0; i < n; i++) {
		mrb_value v = mrb_ary_ref(m, backtrace, i);
		fprintf(f, "\t%s\n", RSTRING_PTR(v));
	}
	fprintf(f, "%s\n", RSTRING_PTR(mrb_inspect(m, mrb_obj_value(m->exc))));
	fclose(f);*/
}
static int script_load(mrb_state *m, const char *filename, mrb_value *rbfile)
{
	FILE *const f = FOPEN(filename, "r");
	if(f == NULL){
		message_printf("script file: %s is not found\n", filename);
		return -1;
	}
	const int arena = mrb_gc_arena_save(m);
	{
		mrbc_context *c = mrbc_context_new(m);
		mrbc_filename(m, c, filename);
		*rbfile = mrb_load_file_cxt(m, f, c);
		mrbc_context_free(m, c);
	}
	fclose(f);
	if(m->exc != 0 && (mrb_nil_p(*rbfile) || mrb_undef_p(*rbfile))){
		my_error_print(m);
		mrb_close(m);
		return -2;
	}
	mrb_gc_arena_restore(m, arena);
	return 0;
}

int script_array_into_uint8_t(mrb_state *m, struct script_array *t)
{
	t->size = ARY_LEN(RARRAY(t->mrb_array));
	if(t->size == 0){
		t->data = NULL;
		return 1;
	}
	t->data = malloc(t->size);
	for(mrb_int i = 0; i < t->size; i++){
		mrb_int v = mrb_fixnum(mrb_ary_ref(m, t->mrb_array, i));
		if(v < 0 || v >= 0x100){
			free(t->data);
			t->data = NULL;
			return 0;
		}
		t->data[i] = v;
	}
	return 1;
}
static mrb_value m_crc32_calc(mrb_state *m, mrb_value self)
{
	struct script_array t;
	mrb_get_args(m, "A", &t.mrb_array);
	if(script_array_into_uint8_t(m, &t) == 0){
		mrb_sys_fail(m, "uint8_t range error");
		return mrb_nil_value();
	}
	if(t.size == 0){
		return mrb_fixnum_value(0);
	}
	uint32_t crc = crc32(0L, Z_NULL, 0);
	crc = crc32(crc, t.data, t.size);
	free(t.data);
	return mrb_fixnum_value(crc);
}

static int script_load_multi(mrb_state *m, const char **b)
{
	mrb_value rbfile;
	int r = 0;
	while(*b != NULL){
		int r = script_load(m, *b, &rbfile);
		if(r != 0){
			return r;
		}
		b++;
	}
	return r;
}
int script_exec(const char **filename, const char *funcname, int argc, char **c_argv, void (*method_define)(mrb_state *m))
{
#define SCRIPTARGNUM (20)
	if(argc > SCRIPTARGNUM){
		message_printf("argc is too many, argc > %d\n", SCRIPTARGNUM);
		return -1;
	}
	mrb_state *m = mrb_open();
	mrb_define_method(m, m->kernel_module, "crc32_calc", m_crc32_calc, 1);
	(*method_define)(m);
	mruby_progressbar_method_define(m);
	mruby_textarea_method_define(m);

	int r = script_load_multi(m, filename);
	if(r != 0){
		return r;
	}
	
	mrb_value m_argv = mrb_ary_new(m);
	for(int i = 0; i < argc; i++){
		mrb_ary_push(m, m_argv, mrb_str_new_cstr(m, c_argv[i]));
	}
	mrb_funcall_argv(m, mrb_top_self(m), mrb_intern_cstr(m, funcname), 1, &m_argv);

	r = 0;
	if(m->exc != 0){
		my_error_print(m);
		//m->exc = 0;
		r = -2;
	}
	mrb_close(m);
	
	return r;
}
