#pragma once
struct textarea;
struct mrb_state;
void mruby_textarea_method_define(struct mrb_state *m);
void message_printf(const char *format, ...);
void message_puts(const char *str);
