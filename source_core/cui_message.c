#include <stdio.h>
#include <hamo.h>
#include "textarea_local.h"

static const struct textarea_callback callback;
void cui_textarea_new(void)
{
	message_new(NULL, &callback, "hamo.log");
}
void cui_textarea_delete(void)
{
	message_delete();
}

static void print(void *t, const char *str)
{
	printf(str);
	fflush(stdout);
}

static const struct textarea_callback callback = {
	.print = print
};


