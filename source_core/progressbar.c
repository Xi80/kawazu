#include <assert.h>
#include <stdint.h>
#include <mruby.h>
#include <hamo.h>
#include "progressbar_local.h"

static struct progressbar{
	void *klass;
	const struct progressbar_callback *m;
	int index;
}global_progressbar;
#define DECLARE_DEVICE(NAME) struct progressbar *const NAME = &global_progressbar

void progressbar_init(void *klass, const struct progressbar_callback *callback)
{
	DECLARE_DEVICE(d);
	d->index = -1;
	d->klass = klass;
	d->m = callback;
}
void progressbar_add(int length)
{
	DECLARE_DEVICE(d);
	if(d->index < 0 || d->index >= 2){
		return;
	}
	d->m->add(d->klass, d->index, length);
}

#include <mruby/string.h>
#include "mruby_driver.h"

static mrb_value m_progressbar_range_set(mrb_state *m, mrb_value self)
{
	mrb_int index, length;
	mrb_value name;
	mrb_get_args(m, "iSi", &index, &name, &length);
	DECLARE_DEVICE(d);
	assert(index == 0 || index == 1);
	d->m->range_set(d->klass, index, RSTRING_PTR(name), length);
	return mrb_nil_value();
}
static mrb_value m_progressbar_draw_index_set(mrb_state *m, mrb_value self)
{
	mrb_int index;
	mrb_get_args(m, "i", &index);
	DECLARE_DEVICE(d);
	d->index = index;
	return mrb_nil_value();
}
static mrb_value m_progressbar_ready(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	if(d->m->ready){
		d->m->ready(d->klass);
	}
	return mrb_nil_value();
}
static mrb_value m_progressbar_done(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	d->index = -1;
	if(d->m->done){
		d->m->done(d->klass);
	}
	return mrb_nil_value();
}
static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
	M(progressbar_range_set, 3),
	M(progressbar_draw_index_set, 1),
	M(progressbar_ready, 0),
	M(progressbar_done, 0),
	{NULL, NULL, 0}
#undef M
};
void mruby_progressbar_method_define(mrb_state *m)
{
	script_method_define(m, list);
}
