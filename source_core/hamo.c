#include <unistd.h>
#include <stdint.h>
#include <hamo.h>
struct mrb_state;
#include "mruby_loader.h"
#include "mruby_simulator.h"
#include "mruby_serialport.h"
#include "textarea_local.h"

static const char *const USAGE[] ={
"hamo (options...) [functionname] (function arguments...)",
"options:",
"  -f [filename] ; load script filename",
"  -s ; enable simulator methods and disable real serial port methods",
NULL
};
int32_t hamo_main(int32_t c, char **v)
{
	void (*method_define)(struct mrb_state *m) = mruby_serialport_method_define;
	int r;
#define SCRIPTFILENUM (20)
	const char *scriptfilename[SCRIPTFILENUM + 1], **s = scriptfilename;

	message_printf(v[0]);
	for(int i = 1; i < c; i++){
		message_printf(" %s", v[i]);
	}
	message_puts("");

	do{
		r = getopt(c, v, "sf:");
		switch(r){
		case 's':
			method_define = simulator_method_define;
			break;
		case 'f':
			if((s - scriptfilename) > SCRIPTFILENUM){
				message_puts("too many script filenames");
				return -1;
			}
			*s = optarg;
			s++;
			break;
		case -1:
			break;
		default:
			r = 1;
			c = -1;
			message_printf("unknown option '%c'\n", r);
			break;
		}
	}while(r != -1);
#undef SCRIPTFILENUM
	*s = NULL;
	if((c - optind) < 1){
		const char *const *u = USAGE;
		while(*u){
			message_puts(*u++);
		}
		return -1;
	}
	v += optind;
	c -= optind;

	const char *funcname = *v;
	v += 1;
	c -= 1;
	r = script_exec(scriptfilename, funcname, c, v, method_define);
	return r;
}
