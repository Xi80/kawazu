#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <hamo.h>
#include "textencoding.h"

static int towidechar(UINT codepage, const char *src, wchar_t **dest)
{
	const int wl = MultiByteToWideChar(codepage, 0, src, -1, NULL, 0);
	assert(wl > 0);
	*dest = malloc(wl * sizeof(wchar_t));
	const int wwl = MultiByteToWideChar(codepage, 0, src, -1, *dest, wl);
	assert(wl == wwl);
	return wl;
}

char *defaultcodepage_to_utf8(const char *src)
{
	wchar_t *wchar_str;
	const int wl = towidechar(CP_ACP, src, &wchar_str);
	const int el = WideCharToMultiByte(CP_UTF8, 0, wchar_str, wl, NULL, 0, NULL, 0); 
	char *utf8_str = malloc(el);
	const int eel = WideCharToMultiByte(CP_UTF8, 0, wchar_str, wl, utf8_str, el, NULL, 0); 
	assert(el == eel);
	free(wchar_str);
	return utf8_str;
}

FILE *my_fopen(const char *src, const wchar_t *mode)
{
	wchar_t *wchar_str;
	towidechar(CP_UTF8, src, &wchar_str);
	FILE *f = _wfopen(wchar_str, mode);
	free(wchar_str);
	return f;
}
