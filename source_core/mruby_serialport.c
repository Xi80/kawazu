#include <assert.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <mruby.h>
#include <mruby/string.h>
#include <mruby/array.h>
#include "serialport.h"
#include "mruby_loader.h"
#include "mruby_driver.h"

static mrb_value m_usleep(mrb_state *m, mrb_value self)
{
	mrb_int d;
	mrb_get_args(m, "i", &d);
	usleep(d);
	return mrb_nil_value();
}
#define ARRAY_SIZE (0x10000)
static struct global_device{
	struct serialport *serialport;
	uint8_t *array_u8;
	mrb_value *array_mrb;
}global_device;
#define DECLARE_DEVICE(NAME) struct global_device *const NAME = &global_device

static mrb_value m_serialport_open(mrb_state *m, mrb_value self)
{
	mrb_value portname;
	mrb_get_args(m, "S", &portname);
	DECLARE_DEVICE(d);
	d->serialport = serialport_create(RSTRING_PTR(portname), 3000000, SERIAL_PARITY_NONE, 8);
	if(d->serialport == NULL){
		return mrb_false_value();
	}
	assert(d->array_u8 == NULL);
	d->array_u8 = malloc(ARRAY_SIZE * sizeof(uint8_t));
	assert(d->array_mrb == NULL);
	d->array_mrb = malloc(ARRAY_SIZE * sizeof(mrb_value));
	return mrb_true_value();
}
static mrb_value m_serialport_close(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	if(d->serialport == NULL){
		return mrb_nil_value();
	}
	serialport_destroy(d->serialport);
	d->serialport = NULL;
	free(d->array_u8);
	d->array_u8 = NULL;
	free(d->array_mrb);
	d->array_mrb = NULL;
	return mrb_nil_value();
}
static mrb_value m_serialport_send(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	assert(d->serialport);
	struct script_array t;
	mrb_get_args(m, "A", &t.mrb_array);
	int r = script_array_into_uint8_t(m, &t);
	assert(r); //TBD: 0 が戻る時の対応
#ifdef NDEBUG
	r++;
#endif
	serialport_send(d->serialport, t.data, t.size);
	return mrb_nil_value();
}
static mrb_value m_serialport_receive(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	assert(d->serialport);
	mrb_int length;
	mrb_get_args(m, "i", &length);
	assert(length <= ARRAY_SIZE);
	length = serialport_receive(d->serialport, d->array_u8, length);
	for(int i = 0; i < length; i++){
		d->array_mrb[i] = mrb_int_value(m, d->array_u8[i]);
	}
	mrb_value ar = mrb_ary_new_from_values(m, length, d->array_mrb);
	return ar;
}
static mrb_value m_serialport_rx_purge(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	assert(d->serialport);
	serialport_rx_purge(d->serialport);
	return mrb_nil_value();
}
static mrb_value m_serialport_receive_timeout_set(mrb_state *m, mrb_value self)
{
	DECLARE_DEVICE(d);
	assert(d->serialport);
	mrb_int ms;
	mrb_get_args(m, "i", &ms);
	serialport_receive_timeout_set(d->serialport, ms);
	return mrb_nil_value();
}
static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
	M(usleep, 1),
	M(serialport_open, 1), M(serialport_close, 0),
	M(serialport_send, 1), 
	M(serialport_receive, 1),
	M(serialport_rx_purge, 0),
	M(serialport_receive_timeout_set, 1),
	{NULL, NULL, 0}
#undef M
};

void mruby_serialport_method_define(mrb_state *m)
{
	script_method_define(m, list);
}
