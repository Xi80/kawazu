#include <assert.h>
#include <mruby.h>
#include <mruby/hash.h>
#include <usb_driver.h>
#include "mruby_driver.h"
#include "mruby_simulator.h"

static struct usb_driver *global_usb_driver = NULL;
static mrb_value global_simulator_hash;

static mrb_value m_simulator_init(mrb_state *m, mrb_value self)
{
	global_usb_driver = usb_driver_init(m);
	mrb_get_args(m, "H", &global_simulator_hash);
	return mrb_nil_value();
}
static mrb_value m_serialport_send(mrb_state *m, mrb_value self)
{
	assert(global_usb_driver);
	struct script_array t;
	mrb_get_args(m, "A", &t.mrb_array);
	int r = script_array_into_uint8_t(m, &t);
	assert(r); //TBD: 0 が戻る時の対応
#ifdef NDEBUG
	r++;
#endif
	usb_host_to_device(global_usb_driver, t.data, t.size);
	return mrb_nil_value();
}
static mrb_value m_serialport_receive_buffer_get(mrb_state *m, mrb_value self)
{
	return mrb_hash_get(m, global_simulator_hash, mrb_symbol_value(mrb_intern_cstr(m, "receive_data")));
}

void simulator_task(struct usb_driver *t, mrb_value h); //usb_driver.c
static mrb_value m_simulator_task(mrb_state *m, mrb_value self)
{
	assert(global_usb_driver);
	simulator_task(global_usb_driver, global_simulator_hash);
	return mrb_nil_value();
}

static const struct script_function_list list[] = {
#define M(NAME, ARGC) {#NAME, m_##NAME, ARGC}
	M(serialport_send, 1), M(serialport_receive_buffer_get, 0), 
	M(simulator_task, 0), 
	M(simulator_init, 1),
	{NULL, NULL, 0}
#undef M
};
void simulator_method_define(struct mrb_state *m)
{
	script_method_define(m, list);
}
