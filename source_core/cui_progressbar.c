#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <hamo.h>
#include "progressbar_local.h"

struct cui_progressbar{
	struct bar{
		char name[20];
		int length, offset;
	}bar[2];
};
static struct bar *bar_get(struct cui_progressbar *t, int index)
{
	assert(index < 2);
	struct bar *b = t->bar + index;
	return b;
}
static void cui_range_set(void *tt, int32_t index, const char *name, int32_t length)
{
	struct bar *b = bar_get(tt, index);
	strncpy(b->name, name, sizeof(b->name) - 1);
	b->length = length;
	b->offset = 0;
}
static int percent(struct bar *b)
{
	int r = 100 * b->offset;
	assert(b->length);
	return r / b->length;
}
static void draw(struct cui_progressbar *t)
{
	if(t->bar[1].length == 0){
		printf("\r%s %3d%%;%s", 
			t->bar[0].name, 
			percent(&t->bar[0]), 
			t->bar[1].name
		);
	}else{
		printf("\r%s %3d%%;%s %3d%%",
			t->bar[0].name, 
			percent(&t->bar[0]), 
			t->bar[1].name,
			percent(&t->bar[1])
		);
	}
	fflush(stdout);
}
static void cui_add(void *tt, int32_t index, int32_t length)
{
	struct cui_progressbar *t = tt;
	struct bar *b = bar_get(t, index);
	b->offset += length;
	assert(b->offset <= b->length);
	draw(t);
}
static void cui_ready(void *t)
{
	draw(t);
}
static void cui_done(void *tt)
{
	struct cui_progressbar *t = tt;
	int r = 1 + strlen(t->bar[0].name);
	r += 1 + 3 + 1;
	r += strlen(t->bar[1].name);
	if(t->bar[1].length != 0){
		r += 1 + 3 + 1;
	}
	char str[r + 1 + 1];
	memset(str, ' ', r + 1 + 1);
	str[0] = '\r';
	str[r] = '\r';
	str[r + 1] = '\0';
	printf(str);
	fflush(stdout);
}
static const struct progressbar_callback cui_progress_callback = {
	.range_set = cui_range_set, .add = cui_add, .ready = cui_ready, .done = cui_done
};

void cui_progressbar_init(void)
{
	static struct cui_progressbar x;
	memset(&x, 0, sizeof(x));
	progressbar_init(&x, &cui_progress_callback);
}
