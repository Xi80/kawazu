#pragma once

struct serialport;
enum serialport_parity{
	SERIAL_PARITY_NONE, SERIAL_PARITY_EVEN, SERIAL_PARITY_ODD
};
struct serialport *serialport_create(const char *portname, uint32_t baud, enum serialport_parity parity, int byteperbit);
void serialport_destroy(struct serialport *t);
void serialport_rx_purge(struct serialport *t);
void serialport_receive_timeout_set(struct serialport *const t, int ms);
uint32_t serialport_send(struct serialport *t, const uint8_t *buf, uint32_t length);
uint32_t serialport_receive(struct serialport *t, uint8_t *buf, uint32_t length);

