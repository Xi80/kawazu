#pragma once

struct script_array{
	mrb_value mrb_array;
	mrb_int size;
	uint8_t *data;
};
int script_array_into_uint8_t(mrb_state *m, struct script_array *t);
struct script_function_list{
	const char *name;
	mrb_value (*func)(struct mrb_state *m, mrb_value self);
	int argc;
};
void script_method_define(struct mrb_state *m, const struct script_function_list *l);
