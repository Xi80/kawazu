#pragma once
#if defined(_WIN32) || defined(_WIN64)
DLLFUNC char *defaultcodepage_to_utf8(const char *src);
FILE *my_fopen(const char *src, const wchar_t *mode);
#define FOPEN(_filename_, _mode_) my_fopen(_filename_, L ## _mode_)
#else
#define FOPEN(_filename_, _mode_) fopen(_filename_, _mode_)
#endif
