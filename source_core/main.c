#include <stdint.h>
#if defined(_WIN32) || defined(_WIN64)
#include <stdlib.h>
#include <stdio.h>
#endif
#include <hamo.h>
struct mrb_state;
#include "textencoding.h"
#include "cui_progressbar.h"
#include "cui_message.h"

int main(const int c, char **const v)
{
#if defined(_WIN32) || defined(_WIN64)
	for(int i = 0; i < c; i++){
		v[i] = defaultcodepage_to_utf8(v[i]);
	}
#endif
	cui_progressbar_init();
	cui_textarea_new();
	const int r = hamo_main(c, v);
	cui_textarea_delete();
#if defined(_WIN32) || defined(_WIN64)
	for(int i = 0; i < c; i++){
		free(v[i]);
	}
#endif
	return r;
}
