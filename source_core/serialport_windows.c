/*
注意: これは Windows API 依存のソース.
*/
#include <assert.h>
#include <stdint.h>
#include <stdlib.h> //malloc(), free()
#include <stdio.h> //snprintf()
#include <string.h> //memcmp()
#include <windows.h>
#include "progressbar_local.h"
#include "serialport.h"

struct serialport{
	HANDLE port;
	int receive_timeout_ms;
};

struct serialport *serialport_create(const char *portname, uint32_t baud, enum serialport_parity parity, int byteperbit)
{
	struct serialport *const t = (struct serialport *) malloc(sizeof(struct serialport));
	assert(t);
	memset(t, 0, sizeof(struct serialport));
	t->receive_timeout_ms = 480*6;

	{
		const char prefix[] = "\\\\.\\";
		const char *p = portname;
		char pn[0x18];
		if(memcmp(prefix, portname, sizeof(prefix) - 1) != 0){
			p = pn;
			snprintf(pn, sizeof(pn)-1, "%s%s", prefix, portname);
		}
		t->port = CreateFile(p, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
	}

	{
		DCB d;
		GetCommState(t->port, &d);
		d.BaudRate = baud;
		switch(parity){
		case SERIAL_PARITY_NONE:
			d.Parity = NOPARITY;
			d.fParity = 0;
			break;
		case SERIAL_PARITY_EVEN:
			d.Parity = EVENPARITY;
			d.fParity= 1;
			break;
		case SERIAL_PARITY_ODD:
			d.Parity = ODDPARITY;
			d.fParity = 1;
			break;
		}
		d.StopBits = ONESTOPBIT;
		d.ByteSize = byteperbit;
		
		if(SetCommState(t->port, &d) == FALSE){
			free(t);
			return NULL;
		}
	}

	{
		COMMTIMEOUTS timeout;
		memset(&timeout, 0, sizeof(COMMTIMEOUTS));
		timeout.ReadIntervalTimeout = t->receive_timeout_ms;
		if(SetCommTimeouts(t->port, &timeout) == FALSE){
			free(t);
			return NULL;
		}
	}
	return t;
}

void serialport_destroy(struct serialport *const t)
{
	CloseHandle(t->port);
	free(t);
}

void serialport_rx_purge(struct serialport *t)
{
	BOOL r = PurgeComm(t->port, PURGE_RXCLEAR);
	assert(r == TRUE);
}

struct rwex{
	OVERLAPPED ov; //cast のため先頭に置くこと
	volatile DWORD length;
};
static void file_callback(DWORD error, DWORD length, OVERLAPPED *ov)
{
	assert(error == 0);
	struct rwex *const s = (struct rwex *) ov;
	s->length = length;
	progressbar_add(length);
}
uint32_t serialport_send(struct serialport *const t, const uint8_t *buf, uint32_t length)
{
	uint32_t sent = 0;
	struct rwex s;
	memset(&s, 0, sizeof(s));
	while(length){
		s.length = 0;
		BOOL r = WriteFileEx(t->port, buf, length, &s.ov, file_callback);
		assert(r == TRUE);
		SleepEx(INFINITE, TRUE);
		assert(s.length);
		buf += s.length;
		sent += s.length;
		length -= s.length;
	}
	return sent;
}

void serialport_receive_timeout_set(struct serialport *const t, int ms)
{
	t->receive_timeout_ms = ms;
}

uint32_t serialport_receive(struct serialport *const t, uint8_t *buf, uint32_t length)
{
	uint32_t red = 0;
	struct rwex s;
	memset(&s, 0, sizeof(s));
	while(length){
		s.length = 0;
		BOOL r = ReadFileEx(t->port, buf, length, &s.ov, file_callback);
		assert(r == TRUE);
		if(SleepEx(t->receive_timeout_ms, TRUE) == 0){
			return red;
		}
		if(s.length == 0){
			return red;
		}
		buf += s.length;
		red += s.length;
		length -= s.length;
	}
	return red;
}
